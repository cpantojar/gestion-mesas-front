import React, {Suspense} from 'react';
import {
    Route,
    Switch,
    BrowserRouter, Redirect,
} from 'react-router-dom';
import {IntlProvider} from 'react-intl';
import AppLocale from "./utils/lang";
import {NotificationContainer} from "./components/common/react-notifications";
import {adminRoot} from "./utils/constants/defaultValues";
import {ProtectedRoute} from "./utils/helpers/authHelper";


const ViewDashboard = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/dashboard')
);

const ViewUsers = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/users')
);

const ViewCategorias = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/categorias')
);

const ViewProductos = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/producto')
);

const ViewMesas = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/mesa')
);

const ViewCaja = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/caja')
);

const ViewPedido = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/pedido')
);

const ViewRestaurante = React.lazy(() =>
    import(/* webpackChunkName: "views-dashboard" */ './pages/restaurante')
);

const ViewError = React.lazy(() =>
    import(/* webpackChunkName: "views-error" */ '../src/pages/error')
);
const ViewUnauthorized = React.lazy(() =>
    import(/* webpackChunkName: "views-error" */ '../src/pages/unauthorized')
);

const ViewLogin = React.lazy(() =>
    import(/* webpackChunkName: "views-user" */ '../src/pages/user/login')
);

const ViewEntregas = React.lazy(() =>
    import(/* webpackChunkName: "views-user" */ '../src/pages/chofer')
);

const ViewDelivery = React.lazy(() =>
    import(/* webpackChunkName: "views-user" */ '../src/pages/delivery')
);
function MainRouter() {
    const currentAppLocale = AppLocale['es'];
    return (
        <div className="h-100">
            <IntlProvider
                locale={currentAppLocale.locale}
                messages={currentAppLocale.messages}>
                <>
                    <NotificationContainer/>
                    <Suspense fallback={<div className="loading"/>}>
                        <BrowserRouter>
                            <Switch>
                                <ProtectedRoute
                                    path={adminRoot}
                                    component={ViewDashboard}
                                />
                                <ProtectedRoute
                                    path="/usuarios"
                                    component={ViewUsers}
                                />

                                <ProtectedRoute
                                    path="/categorias"
                                    component={ViewCategorias}
                                />
                                <ProtectedRoute
                                    path="/productos"
                                    component={ViewProductos}
                                />
                                <ProtectedRoute
                                    path="/mesas"
                                    component={ViewMesas}
                                />
                                <ProtectedRoute
                                    path="/pedidos"
                                    component={ViewPedido}
                                />
                                <ProtectedRoute
                                    path="/caja"
                                    component={ViewCaja}
                                />
                                <ProtectedRoute
                                    path="/restaurante"
                                    component={ViewRestaurante}
                                />
                                <ProtectedRoute
                                    path="/entregas"
                                    component={ViewEntregas}
                                />
                                <ProtectedRoute
                                    path="/delivery"
                                    component={ViewDelivery}
                                />
                                <Route
                                    path="/error"
                                    exact
                                    render={(props) => <ViewError {...props} />}
                                />
                                <Route
                                    path="/unauthorized"
                                    exact
                                    render={(props) => <ViewUnauthorized {...props} />}
                                />
                                <Route
                                    path="/"
                                    exact
                                    render={(props) => <ViewLogin {...props} />}
                                />
                                {/*
                  <Redirect exact from="/" to={adminRoot} />
                  */}
                                <Redirect to="/unauthorized"/>
                            </Switch>
                        </BrowserRouter>
                    </Suspense>
                </>
            </IntlProvider>
        </div>
    );
}


export default MainRouter;