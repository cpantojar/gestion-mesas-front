import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid, InputLabel, Switch,
    TextField,
    Typography,
    withStyles
} from "@material-ui/core";
import {Button} from "reactstrap";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as categoriaActions from '../../redux/categoria/action'
import * as userActions from "../../redux/users/actions";
import InputSpinner from "react-bootstrap-input-spinner";

const DEFAULT_FORM = {
    nombre: '',
    codigo: '',
    ruc: '',
    aforo: '',
    estado: true,
    urlFoto: true,
}

function ModalRestaurante({openModal, data}) {
    const {data: categoriaList} = useSelector((state) => state.categoria.list);
    const dispatch = useDispatch();
    const [file, setFile] = useState(null);
    const AntSwitch = withStyles((theme) => ({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: theme.palette.grey[500],
            '&$checked': {
                transform: 'translateX(12px)',
                color: theme.palette.common.white,
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.primary.main,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }))(Switch);
    const [modalValue, setOpenModal] = useState(false)
    const [state, setState] = React.useState({
        checkedC: false,
    });
    const [modalForm, setModalForm] = useState(DEFAULT_FORM)

    useEffect(() => {
        setOpenModal(openModal)
    }, [openModal])

    useEffect(() => {
        console.log(data);
        if (data) {
            setModalForm(data);
        }
    }, [data]);
    const handleCloseModal = () => {
        setOpenModal(false)
    }

    const onSubmitFormModal = () => {
        if (modalForm.id) {
            dispatch(categoriaActions.editCategoria(modalForm, categoriaList))
        } else {
            dispatch(categoriaActions.addNewCategoria(modalForm, categoriaList))

        }
        handleCloseModal()
    }
    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }
    const handleChange = (event, id) => {
        setState(event.target.checked);
        setModalForm((state) => {
            return {
                ...state,
                [id]: event.target.checked
            }
        })
    };
    const onFileLoad = async (data) => {
        const fileType = data.target.files[0]
        // dispatch(clientActions.uploadPhoto('60419756964e564e484502c3', file))
        setFile(fileType)
    }
    return (
        <Dialog open={modalValue} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">
                {modalForm.id ? 'Editar' : 'Agregar'} Categoria
            </DialogTitle>
            <DialogContent>

                <TextField
                    margin="dense"
                    label="Nombre"
                    id='nombre'
                    type="text"
                    fullWidth
                    value={modalForm.nombre}
                    onChange={(val) => onUpdateInput(val, 'nombre')}
                />
                <TextField
                    margin="dense"
                    label="Codigo"
                    id='codigo'
                    type="text"
                    fullWidth
                    value={modalForm.codigo}
                    onChange={(val) => onUpdateInput(val, 'nombre')}
                />
                <TextField
                    margin="dense"
                    label="Ruc"
                    id='ruc'
                    type="text"
                    fullWidth
                    value={modalForm.ruc}
                    onChange={(val) => onUpdateInput(val, 'nombre')}
                />
                <br/><br/>
                <InputLabel id="demo-simple-select-filled-label">Aforo</InputLabel>
                <InputSpinner
                    type={'int'}
                    precision={2}
                    max={1000}
                    min={0}
                    step={1}
                    value={modalForm.aforo ? modalForm.aforo : 1}
                    onChange={num => modalForm.aforo = num}
                    variant={'primary'}
                    size="sm"
                />
                <br/><br/>
                <Typography component="div">
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>Deshabilidado</Grid>
                        <Grid item>
                            <AntSwitch checked={modalForm.estado ? modalForm.estado : state.checkedC}
                                       onChange={(val) => handleChange(val, 'estado')} name="checkedC"/>
                        </Grid>
                        <Grid item>Habilidado</Grid>
                    </Grid>
                </Typography>
                <br/><br/>
                <input type='file' onChange={onFileLoad}/>
                <img src={file} />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseModal} color="primary">
                    Cancelar
                </Button>
                <Button variant="contained" color="primary" onClick={onSubmitFormModal}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>);

}

export default ModalRestaurante