import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    InputLabel,
    MenuItem,
    TextField,
    Select
} from "@material-ui/core";
import {Button} from "reactstrap";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as categoriesAction from "../../redux/categoria/action";
import * as productoActions from '../../redux/producto/actions'
import InputSpinner from 'react-bootstrap-input-spinner'

const DEFAULT_FORM = {
    nombre: '',
    estado: true,
    categoria: null,
    precio: 0.0,
    cantidad: 0,
    fechaVencimiento: '',
}

function ModalProducto({openModal, data}) {
    const {data: productoList} = useSelector((state) => state.producto.list);
    const stateCategorias = useSelector((state) => state.categoria);
    const dispatch = useDispatch();
    const [modalValue, setOpenModal] = useState(false)
    const [categoria, setCategoria] = React.useState('');

    const [modalForm, setModalForm] = useState(DEFAULT_FORM)
    const getCategories = () => {
        dispatch(categoriesAction.getCategoria());
    }
    useEffect(() => {
        getCategories();
    }, [])
    useEffect(() => {
        setOpenModal(openModal)
    }, [openModal])
    useEffect(() => {
        console.log(data);
        if (data) {
            setModalForm(data);
        }
    }, [data]);
    const handleChange = (event) => {
        setCategoria(event.target.value)
    };

    const handleCloseModal = () => {
        setOpenModal(false)
    }

    const onSubmitFormModal = () => {
        modalForm.categoria = categoria;
        dispatch(productoActions.addProducto(modalForm, productoList))
        handleCloseModal()
    }
    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }

    return (
        <Dialog open={modalValue} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">
                Producto
            </DialogTitle>
            <DialogContent>

                <TextField
                    margin="dense"
                    label="Nombre"
                    id='nombre'
                    type="text"
                    fullWidth
                    value={modalForm.nombre}
                    onChange={(val) => onUpdateInput(val, 'nombre')}
                />
                <br/><br/>
                <InputLabel id="demo-simple-select-filled-label">Categoria</InputLabel>

                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    style={{width: '100%'}}
                    value={modalForm.categoria? modalForm.categoria:categoria}
                    placeholder="Seleccionar una opción"
                    onChange={handleChange}
                >
                    {stateCategorias.list.data && stateCategorias.list.data.map((categoria) => (
                        <MenuItem value={categoria} key={categoria.id}>{categoria.nombre} </MenuItem>
                    ))}
                </Select>
                <br/><br/>
                <InputLabel id="demo-simple-select-filled-label">Precio</InputLabel>
                <InputSpinner
                    type={'double'}
                    precision={2}
                    max={1000}
                    min={0}
                    step={0.5}
                    value={modalForm.precio?modalForm.precio:1}
                    onChange={num => modalForm.precio = num}
                    variant={'primary'}
                    size="sm"
                />
                <br/><br/>
                <InputLabel id="demo-simple-select-filled-label">Cantidad</InputLabel>
                <InputSpinner
                    type={'int'}
                    precision={2}
                    max={1000}
                    min={0}
                    step={1}
                    value={modalForm.cantidad?modalForm.cantidad:1}
                    onChange={num => modalForm.cantidad = num}
                    variant={'primary'}
                    size="sm"
                />
                <br/><br/>
                <TextField
                    margin="dense"
                    label="Fecha de Vencimiento"
                    type="date"
                    id='fechaVencimiento'
                    InputLabelProps={{
                        shrink: true,
                    }}
                    fullWidth
                    value={modalForm.fechaVencimiento}
                    onChange={(val) => onUpdateInput(val, 'fechaVencimiento')}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseModal} color="primary">
                    Cancelar
                </Button>
                <Button variant="contained" color="primary" onClick={onSubmitFormModal}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>);

}

export default ModalProducto