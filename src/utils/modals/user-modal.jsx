import {Dialog, DialogActions, DialogContent, DialogTitle, TextField} from "@material-ui/core";
import {Button} from "reactstrap";
import React, {useEffect, useState} from "react";
import {Multiselect} from "multiselect-react-dropdown";
import {useDispatch, useSelector} from "react-redux";
import * as rolesActions from "../../redux/roles/action";
import * as userActions from '../../redux/users/actions'
import {getCurrentUser} from "../helpers/Utils";

const DEFAULT_FORM = {
    mail: '',
    dni: '',
    nombre: '',
    apellidoPaterno: '',
    apellidoMaterno: '',
    foto: '',
    roles: []
}

function ModalUser({openModal, data}) {
    const {data: usersList} = useSelector((state) => state.users.list);
    const [modalValue, setOpenModal] = useState(false)
    const [modalForm, setModalForm] = useState(DEFAULT_FORM)
    const [userRol] = useState(getCurrentUser().roles)
    const stateRoles = useSelector((state) => state.roles);
    const dispatch = useDispatch();
    const restauranteCode = getCurrentUser().restaurante.codigo
    const getRoles = () => {
        dispatch(rolesActions.getRoles());
    }
    const rolesList = () => {
        if(restauranteCode !== 'seven'){
            if(stateRoles.list.data !== null){
                return stateRoles.list.data.filter((p) => p.nombre !== "SEVEN")
            }
        }
        return stateRoles.list.data
    }
    useEffect(() => {
        getRoles();
    }, [])
    useEffect(() => {
        setOpenModal(openModal)
    }, [openModal])
    useEffect(() => {
        if (data) {
            setModalForm(data);
        }
    }, [data]);


    const handleChange = (event) => {
        modalForm.roles = event;
    };

    const handleDelete = (chipToDelete) => {
        modalForm.roles = chipToDelete;
    };
    const handleCloseModal = () => {
        setOpenModal(false)
    }
    const onSubmitFormModal = () => {
        if (modalForm.id) {
            dispatch(userActions.editUser(modalForm, usersList))
        } else {
            dispatch(userActions.addNewUser(modalForm, usersList))
        }
        handleCloseModal()
    }
    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }

    return (
        <Dialog open={modalValue} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">
                {modalForm.id ? 'Editar' : 'Agregar'} Usuario
            </DialogTitle>
            <DialogContent>
                <TextField
                    margin="dense"
                    label="DNI"
                    id='DNI'
                    type="text"
                    fullWidth
                    value={modalForm.dni}
                    onChange={(val) => onUpdateInput(val, 'dni')}
                />
                <TextField
                    margin="dense"
                    label="Nombre"
                    id='nombre'
                    type="text"
                    fullWidth
                    value={modalForm.nombre}
                    onChange={(val) => onUpdateInput(val, 'nombre')}
                />
                <TextField
                    margin="dense"
                    label="Apellido Paterno"
                    id='apellido_paterno'
                    type="text"
                    fullWidth
                    value={modalForm.apellidoPaterno}
                    onChange={(val) => onUpdateInput(val, 'apellidoPaterno')}
                />
                <TextField
                    margin="dense"
                    label="Apellido Materno"
                    id='apellido_materno'
                    type="text"
                    fullWidth
                    value={modalForm.apellidoMaterno}
                    onChange={(val) => onUpdateInput(val, 'apellidoMaterno')}
                />
                <TextField
                    margin="dense"
                    label="Mail"
                    id='mail'
                    type="text"
                    fullWidth
                    value={modalForm.mail}
                    onChange={(val) => onUpdateInput(val, 'mail')}
                />
                <br></br>
                <Multiselect
                    options={rolesList()} // Options to display in the dropdown
                    placeholder="Seleccionar Roles de Usuario"
                    onSelect={handleChange} // Function will trigger on select event
                    onRemove={handleDelete} // Function will trigger on remove event
                    displayValue="nombre" // Property name to display in the dropdown options
                    selectedValues={modalForm.roles.length > 0 ? modalForm.roles : ''}
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseModal} color="primary">
                    Cancelar
                </Button>
                <Button variant="contained" color="primary" onClick={onSubmitFormModal}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>);

}

export default ModalUser