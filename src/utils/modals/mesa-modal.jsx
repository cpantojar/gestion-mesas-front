import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid, InputLabel, Switch,
    TextField,
    Typography,
    withStyles
} from "@material-ui/core";
import {Button} from "reactstrap";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as mesaActions from '../../redux/mesa/actions'
import InputSpinner from "react-bootstrap-input-spinner";

const DEFAULT_FORM = {
    identificador: '',
    habilitado: '',
    capacidad: 0,
    estado:1
}

function ModalMesa({openModal, data}) {
    const {data: mesaList} = useSelector((state) => state.mesas.list);
    const dispatch = useDispatch();
    const AntSwitch = withStyles((theme) => ({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: theme.palette.grey[500],
            '&$checked': {
                transform: 'translateX(12px)',
                color: theme.palette.common.white,
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.primary.main,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }))(Switch);
    const [modalValue, setOpenModal] = useState(false)
    const [state, setState] = React.useState({
        checkedC: false,
    });
    const [modalForm, setModalForm] = useState(DEFAULT_FORM)

    useEffect(() => {
        setOpenModal(openModal)
    }, [openModal])

    useEffect(() => {
        console.log(data);
        if (data) {
            setModalForm(data);
        }
    }, [data]);
    const handleCloseModal = () => {
        setOpenModal(false)
    }

    const onSubmitFormModal = () => {
        if (modalForm.id) {
            dispatch(mesaActions.editMesa(modalForm, mesaList))
        } else {
            dispatch(mesaActions.addMesas(modalForm, mesaList))

        }
        handleCloseModal()
    }
    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }
    const handleChange = (event, id) => {
        setState(event.target.checked);
        setModalForm((state) => {
            return {
                ...state,
                [id]: event.target.checked
            }
        })
    };
    return (
        <Dialog open={modalValue} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">
                {modalForm.id ? 'Editar' : 'Agregar'} Mesa
            </DialogTitle>
            <DialogContent>

                <TextField
                    margin="dense"
                    label="Identificador"
                    id='identificador'
                    type="text"
                    fullWidth
                    value={modalForm.identificador}
                    onChange={(val) => onUpdateInput(val, 'identificador')}
                />
                <br/><br/>
                <InputLabel id="demo-simple-select-filled-label">Capacidad</InputLabel>
                <InputSpinner
                    type={'int'}
                    precision={2}
                    max={1000}
                    min={0}
                    step={1}
                    value={modalForm.capacidad ? modalForm.capacidad : 1}
                    onChange={num => modalForm.capacidad = num}
                    variant={'primary'}
                    size="sm"
                />
                <br/><br/>
                <Typography component="div">
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>Deshabilidado</Grid>
                        <Grid item>
                            <AntSwitch checked={modalForm.habilitado ? modalForm.habilitado : state.checkedC}
                                       onChange={(val) => handleChange(val, 'habilitado')} name="checkedC"/>
                        </Grid>
                        <Grid item>Habilidado</Grid>
                    </Grid>
                </Typography>

            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseModal} color="primary">
                    Cancelar
                </Button>
                <Button variant="contained" color="primary" onClick={onSubmitFormModal}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>);

}

export default ModalMesa