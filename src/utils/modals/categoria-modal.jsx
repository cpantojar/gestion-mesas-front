import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Grid, Switch,
    TextField,
    Typography,
    withStyles
} from "@material-ui/core";
import {Button} from "reactstrap";
import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as categoriaActions from '../../redux/categoria/action'
import * as userActions from "../../redux/users/actions";

const DEFAULT_FORM = {
    nombre: '',
    estado: true,
}

function ModalCategoria({openModal, data}) {
    const {data: categoriaList} = useSelector((state) => state.categoria.list);
    const dispatch = useDispatch();
    const AntSwitch = withStyles((theme) => ({
        root: {
            width: 28,
            height: 16,
            padding: 0,
            display: 'flex',
        },
        switchBase: {
            padding: 2,
            color: theme.palette.grey[500],
            '&$checked': {
                transform: 'translateX(12px)',
                color: theme.palette.common.white,
                '& + $track': {
                    opacity: 1,
                    backgroundColor: theme.palette.primary.main,
                    borderColor: theme.palette.primary.main,
                },
            },
        },
        thumb: {
            width: 12,
            height: 12,
            boxShadow: 'none',
        },
        track: {
            border: `1px solid ${theme.palette.grey[500]}`,
            borderRadius: 16 / 2,
            opacity: 1,
            backgroundColor: theme.palette.common.white,
        },
        checked: {},
    }))(Switch);
    const [modalValue, setOpenModal] = useState(false)
    const [state, setState] = React.useState({
        checkedC: false,
    });
    const [modalForm, setModalForm] = useState(DEFAULT_FORM)

    useEffect(() => {
        setOpenModal(openModal)
    }, [openModal])

    useEffect(() => {
        console.log(data);
        if (data) {
            setModalForm(data);
        }
    }, [data]);
    const handleCloseModal = () => {
        setOpenModal(false)
    }

    const onSubmitFormModal = () => {
        if (modalForm.id) {
            dispatch(categoriaActions.editCategoria(modalForm, categoriaList))
        } else {
            dispatch(categoriaActions.addNewCategoria(modalForm, categoriaList))

        }
        handleCloseModal()
    }
    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }
    const handleChange = (event, id) => {
        setState(event.target.checked);
        setModalForm((state) => {
            return {
                ...state,
                [id]: event.target.checked
            }
        })
    };
    return (
        <Dialog open={modalValue} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
            <DialogTitle id="form-dialog-title">
                {modalForm.id ? 'Editar' : 'Agregar'} Categoria
            </DialogTitle>
            <DialogContent>

                <TextField
                    margin="dense"
                    label="Nombre"
                    id='nombre'
                    type="text"
                    fullWidth
                    value={modalForm.nombre}
                    onChange={(val) => onUpdateInput(val, 'nombre')}
                />
                <br/><br/>
                <Typography component="div">
                    <Grid component="label" container alignItems="center" spacing={1}>
                        <Grid item>Deshabilidado</Grid>
                        <Grid item>
                            <AntSwitch checked={modalForm.estado ? modalForm.estado : state.checkedC}
                                       onChange={(val) => handleChange(val, 'estado')} name="checkedC"/>
                        </Grid>
                        <Grid item>Habilidado</Grid>
                    </Grid>
                </Typography>

            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseModal} color="primary">
                    Cancelar
                </Button>
                <Button variant="contained" color="primary" onClick={onSubmitFormModal}>
                    Guardar
                </Button>
            </DialogActions>
        </Dialog>);

}

export default ModalCategoria