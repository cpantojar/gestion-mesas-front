import {adminRoot, UserRole} from './defaultValues';

const data = [
    {
        id: 'dashboard',
        icon: 'home_icon',
        label: 'menu.dashboard',
        to: `/dashboard`,
        roles: [ UserRole.ADMINISTRADOR, ],
    },
    {
        id: 'usuario',
        icon: 'customer_icon',
        label: 'menu.usuarios',
        to: `/usuarios`,
        roles: [ UserRole.ADMINISTRADOR, ],
    },
    {
        id: 'categoria',
        icon: 'category_icon',
        label: 'menu.categoria',
        to: `/categorias`,
        roles: [ UserRole.ADMINISTRADOR, ],
    },
    {
        id: 'producto',
        icon: 'product_icon',
        label: 'menu.producto',
        to: `/productos`,
        roles: [ UserRole.ADMINISTRADOR, ],
    },
    {
        id: 'mesas',
        icon: 'tables_icon',
        label: 'menu.mesa',
        to: `/mesas`,
        roles: [ UserRole.ADMINISTRADOR, ],
    },
    {
        id: 'pedidos',
        icon: 'dish_icon',
        label: 'menu.pedido',
        to: `/pedidos`,
        roles: [UserRole.MOZO, UserRole.ADMINISTRADOR],
    },
    {
        id: 'seven',
        icon: 'restaurante_icon',
        label: 'menu.empresa',
        to: `/restaurante`,
        roles: [UserRole.SEVEN],
    },
    {
        id: 'caja',
        icon: 'receipt_icon',
        label: 'menu.caja',
        to: `/caja`,
        roles: [UserRole.CAJERO, UserRole.ADMINISTRADOR],
    },
    {
        id: 'chofer',
        icon: 'chofer_icon',
        label: 'menu.chofer',
        to: `/entregas`,
        roles: [UserRole.CHOFER, UserRole.ADMINISTRADOR],
    },
    {
        id: 'delivery',
        icon: 'delivery_icon',
        label: 'menu.delivery',
        to: `/delivery`,
        roles: [UserRole.MOZO, UserRole.ADMINISTRADOR],
    },



];
export default data;
