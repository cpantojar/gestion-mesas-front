import React from "react";

const cols = [

    {
        Header: 'Nombre',
        accessor: 'nombre',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },



    {
        Header: 'Categoria',
        accessor: 'categoria',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },

    {
        Header: 'Precio',
        accessor: 'precio',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },

    {
        Header: 'Stock',
        accessor: 'cantidad',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },
    {
        Header: 'Estado',
        accessor: 'estado',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },

];

export default cols