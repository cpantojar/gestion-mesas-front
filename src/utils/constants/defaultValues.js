export const UserRole = {
    ADMINISTRADOR: "ADMINISTRADOR",
    CAJERO: "CAJERO",
    SEVEN: "SEVEN",
    MOZO: "MOZO",
    COCINERO: "COCINERO",
    CHOFER: "CHOFER"
};

/*
Menu Types:
"menu-default", "menu-sub-hidden", "menu-hidden"
*/
export const defaultMenuType = 'menu-default';

export const subHiddenBreakpoint = 1440;
export const menuHiddenBreakpoint = 768;
export const defaultLocale = 'en';
export const localeOptions = [
    {id: 'en', name: 'English - LTR', direction: 'ltr'},
    {id: 'es', name: 'Español', direction: 'ltr'},
    {id: 'enrtl', name: 'English - RTL', direction: 'rtl'},
];

export const adminRoot = '/dashboard';

/*export const currentUser = {
  id: 1,
  title: 'Milagritos Angeles',
  img: '/assets/img/profiles/l-1.jpg',
  date: 'Last seen today 15:24',
  role: UserRole.Admin,
};*/
// cookies variables
export const TOKEN_NAME = 'token'
export const USER_COOKIE = 'user'
// end cooles variables
export const defaultDirection = 'ltr';

// endpoints
