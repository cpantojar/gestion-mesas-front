import React from "react";

const cols = [
    
    {
        label: 'DNI',
        id: 'dni',
        minWidth: 170
    },
    {
        label: 'Nombre',
        id: 'nombre',
        minWidth: 170
    },
    {
        label: 'Apellido Paterno',
        id: 'apellidoPaterno',
        minWidth: 170
    },
    {
        label: 'Apellido Materno',
        id: 'apellidoMaterno',
        minWidth: 170
    },
    {
        label: 'Correo',
        id: 'mail',
        minWidth: 170
    },
    {
        label: 'Estado',
        id: 'estado',
        minWidth: 170
    },

];

export default cols