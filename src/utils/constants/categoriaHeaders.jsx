import React from "react";

const cols = [

    {
        Header: 'Nombre',
        accessor: 'nombre',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },

    {
        Header: 'Estado',
        accessor: 'estado',
        cellClass: 'text-muted w-15',
        Cell: (props) => <>{props.value}</>,
        sortType: 'basic',
    },

];

export default cols