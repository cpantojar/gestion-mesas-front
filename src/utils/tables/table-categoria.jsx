import {
    Chip,
    IconButton, Paper, Snackbar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow
} from "@material-ui/core";
import {Block, Check, Delete, Edit} from "@material-ui/icons";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect, useState} from "react";
import ModalCategoria from "../modals/categoria-modal";
import CloseIcon from "@material-ui/icons/Close";

function TableCategoria({columns, data}) {
    const state = useSelector((state) => state.categoria);
    const [listCategoria] = useState({...data})
    const [openModal, setOpenModal] = useState(false)
    const [categoriaData, setCategoriaData] = useState()
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const [userId, setUserId] = useState()
    const dispatch = useDispatch();
    const [snackBarStatus, setSnackBarStatus] = useState(false)
    const [snackBarMessage, setSnackBarMessage] = useState()

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    useEffect(() => {
        setSnackBarStatus(state.list.snackBar);
        setSnackBarMessage(state.list.message)
    }, [state])
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    const onEdit = (data) => {
        const newForm = {
            id: data.id,
            nombre: data.nombre,
            estado: data.estado,
        }
        setCategoriaData(newForm)
        setOpenModal(true)
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackBarStatus(false);
    };
    return (
        <>
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    key={'nombre'}
                                    style={{minWidth: '170'}}
                                >
                                    Nombre
                                </TableCell>

                                <TableCell
                                    key={'estado'}
                                    style={{minWidth: '170'}}
                                >
                                    Estado
                                </TableCell>

                                <TableCell
                                    key={'accion'}
                                    style={{minWidth: '170'}}
                                >
                                    Accion
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {listCategoria.list.data.map((row) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}
                                              style={{background: !row.estado ? '#AAB7B8' : ''}}>

                                        <TableCell>
                                            {row.nombre}
                                        </TableCell>
                                        <TableCell>
                                            {row.estado ? <Check/> : <Block/>}
                                        </TableCell>
                                        <TableCell>
                                            <IconButton color="primary" edge="end" aria-label="edit"
                                                        onClick={() => onEdit(row)}>
                                                <Edit/>
                                            </IconButton>
                                            <IconButton color="secondary" edge="end" aria-label="delete">
                                                <Delete/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={listCategoria.list.data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            <ModalCategoria openModal={openModal} data={categoriaData} users={listCategoria}/>

            <Snackbar open={snackBarStatus} autoHideDuration={7000} onClose={handleClose}
                      message={<span id="snackbar-fab-message-id"> {snackBarMessage} </span>}
                      action={[
                          <IconButton key="close" aria-label="Close" color="inherit" onClick={handleClose}>
                              <CloseIcon/>
                          </IconButton>,
                      ]}/>
        </>
    );
}

export default TableCategoria