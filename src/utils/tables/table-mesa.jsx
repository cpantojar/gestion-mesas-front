import {
    IconButton, Paper, Snackbar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow
} from "@material-ui/core";
import {Block, Check, Delete, Edit} from "@material-ui/icons";
import React, {useState, useEffect} from "react";
import ModalMesa from "../modals/mesa-modal";
import CloseIcon from "@material-ui/icons/Close";

function TableMesa({data}) {
    const [lista] = useState({...data})
    const [openModal, setOpenModal] = useState(false)
    const [dataMesa, setMesaData] = useState()
    const [rowsPerPage, setRowsPerPage] = React.useState(10)
    const [page, setPage] = React.useState(0)
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const [snackBarStatus, setSnackBarStatus] = useState(false)
    const [snackBarMessage, setSnackBarMessage] = useState()

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    const onEdit = (data) => {
        const newForm = {
            id: data.id,
            identificador: data.identificador,
            estado: data.estado,
            capacidad: data.capacidad,
        }
        setMesaData(newForm)
        setOpenModal(true)
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackBarStatus(false);
    };
    return (
        <>
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    key={'identificador'}
                                    style={{minWidth: '170'}}
                                >
                                    Identificador
                                </TableCell>

                                <TableCell
                                    key={'capacidad'}
                                    style={{minWidth: '170'}}
                                >
                                    Capacidad
                                </TableCell>

                                <TableCell
                                    key={'estado'}
                                    style={{minWidth: '170'}}
                                >
                                    Estado
                                </TableCell>

                                <TableCell
                                    key={'accion'}
                                    style={{minWidth: '170'}}
                                >
                                    Accion
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {lista.list.data.map((row) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}
                                              style={{background: !row.estado ? '#AAB7B8' : ''}}>

                                        <TableCell>
                                            {row.identificador}
                                        </TableCell>
                                        <TableCell>
                                            {row.capacidad}
                                        </TableCell>
                                        <TableCell>
                                            {row.estado ? <Check/> : <Block/>}
                                        </TableCell>
                                        <TableCell>
                                            <IconButton color="primary" edge="end" aria-label="edit"
                                                        onClick={() => onEdit(row)}>
                                                <Edit/>
                                            </IconButton>
                                            <IconButton color="secondary" edge="end" aria-label="delete">
                                                <Delete/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={lista.list.data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            <ModalMesa openModal={openModal} data={dataMesa}/>

            <Snackbar open={snackBarStatus} autoHideDuration={7000} onClose={handleClose}
                      message={<span id="snackbar-fab-message-id"> {snackBarMessage} </span>}
                      action={[
                          <IconButton key="close" aria-label="Close" color="inherit" onClick={handleClose}>
                              <CloseIcon/>
                          </IconButton>,
                      ]}/>

        </>
    );
}

export default TableMesa