import {
    Button,
    Chip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, Paper,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow
} from "@material-ui/core";
import {Block, Check, PictureAsPdf} from "@material-ui/icons";
import React, {useState} from "react";
import {Page, Text, Document, StyleSheet, PDFViewer} from '@react-pdf/renderer';
import {useDispatch, useSelector} from "react-redux";
import * as pedidosAction from "../../redux/pedido/actions";
import * as mesaActions from "../../redux/mesa/actions";
// Create styles for PDF
const styles = StyleSheet.create({
    body: {
        paddingTop: 35,
        paddingBottom: 65,
        paddingHorizontal: 35,
    },
    title: {
        fontSize: 24,
        textAlign: 'center',
    },
    author: {
        fontSize: 12,
        marginBottom: 40,
        textAlign: 'center',
    },
    subTitle: {
        backgroundColor: '#f3f4f5',
        color: '#6435c9',
        fontSize: 16,
        fontWeight: 'bold',
        margin: 12,
        padding: 10,
    },
    text: {
        fontSize: 12,
        marginTop: 5,
        marginBottom: 5,
        marginLeft: 25,
        textAlign: 'justify',
    },
    textItem: {
        fontSize: 10,
        marginLeft: 25,
        marginTop: 5,
        marginBottom: 5,
        textAlign: 'justify',
    },
    price: {
        fontSize: 16,
        color: '#6435c9',
        fontWeight: 'bold',
        textAlign: 'right',
        marginTop: 12,
    },
    header: {
        fontSize: 12,
        marginBottom: 20,
        textAlign: 'center',
        color: 'grey',
    },
});

function TablePedido({data}) {
    const [lista] = useState({...data})
    const [openModal, setOpenModal] = useState(false)
    const [openModalBoleta, setOpenModalBoleta] = useState(false)
    const [openModalConfirmation, setOpenModalConfirmation] = useState(false)
    const pedidos = useSelector((state) => state.pedidosReducer)
    const [detallePedido, setDetallePedido] = useState([])
    const [rowsPerPage, setRowsPerPage] = React.useState(10)
    const [page, setPage] = React.useState(0)
    const [pedido, setPedido] = React.useState('')
    const [idPedido, setIdPedido] = React.useState('')
    const dispatch = useDispatch()

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
        setPage(0);
    };

    const onOpenModal = (items) => {
        setDetallePedido(items)
        setOpenModal(true)
    }

    const handleCloseModal = () => {
        setOpenModal(false)
    }
    const getTotal = (items) => {
        const resultado = items.reduce((acc, el) => {
            return (acc += (el.cantidad * el.producto.precio))
        }, 0)
        return resultado;
    }

    const handleCloseModalBoleta = () => {
        setOpenModalBoleta(false)
    }
    const viewInvoice = (pedido) => {
        setPedido(pedido)
        setOpenModalBoleta(true)
    }
    const openConfirmation = (idPedido) => {
        setIdPedido(idPedido)
        setOpenModalConfirmation(true)
    }
    const hableCloseModalConfirmation = () => {
        setOpenModalConfirmation(false);
    }
    const pagarPedido = () => {
        console.log(idPedido)
        dispatch(pedidosAction.payPedido(idPedido, pedidos))
        setOpenModalConfirmation(false)
    }

    return (
        <>
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell key={'dniCliente'} style={{minWidth: '170'}}>
                                    DNI Cliente
                                </TableCell>

                                <TableCell key={'mesa'} style={{minWidth: '170'}}>
                                    Mesa
                                </TableCell>

                                <TableCell key={'productos'} style={{minWidth: '170'}}>
                                    Productos
                                </TableCell>

                                <TableCell key={'total'} style={{minWidth: '170'}}>
                                    Total
                                </TableCell>
                                <TableCell key={'estado'} style={{minWidth: '170'}}>
                                    Boleta
                                </TableCell>
                                <TableCell key={'estado'} style={{minWidth: '170'}}>
                                    Estado
                                </TableCell>

                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {pedidos.list.data.filter((pedido) => pedido.tipo !== 2).map((row) => {
                                return (
                                    <TableRow key={row.id}>
                                        <TableCell>
                                            {row.dniCliente}
                                        </TableCell>
                                        <TableCell>
                                            {row.mesa.identificador}
                                        </TableCell>
                                        <TableCell>
                                            <Chip
                                                onClick={() => onOpenModal(row.items)}
                                                label={row.items.length} color="primary"/>
                                        </TableCell>
                                        <TableCell>
                                            {getTotal(row.items)}
                                        </TableCell>
                                        <TableCell>
                                            <IconButton color="secondary" edge="end" aria-label="edit"
                                                        onClick={() => viewInvoice(row)}>
                                                <PictureAsPdf/>
                                            </IconButton>
                                        </TableCell>
                                        <TableCell>
                                            {row.estado ? <Chip
                                                label="Pagado"
                                                color="primary"
                                                deleteIcon={<Check/>}
                                            /> : <IconButton
                                                onClick={() => openConfirmation(row.id)}>
                                                <Block/>
                                            </IconButton>}
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={lista.list.data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            <Dialog open={openModal} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    Detalle Pedido
                </DialogTitle>
                <DialogContent>
                    <TableContainer>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    <TableCell key={'cantidad'} style={{minWidth: '170'}}>
                                        Cantidad
                                    </TableCell>
                                    <TableCell key={'desc'} style={{minWidth: '170'}}>
                                        Desc
                                    </TableCell>
                                    <TableCell key={'un'} style={{minWidth: '170'}}>
                                        Prec. Un
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {detallePedido.map((row) => {
                                    return (
                                        <TableRow key={row.producto.id}>
                                            <TableCell>
                                                {row.cantidad}
                                            </TableCell>
                                            <TableCell>
                                                {row.producto.nombre}
                                            </TableCell>
                                            <TableCell>
                                                {row.producto.precio}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseModal} color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>

            <Dialog
                fullWidth={true}
                maxWidth='lg'
                open={openModalBoleta}
                onClose={handleCloseModalBoleta}
                aria-labelledby="max-width-dialog-title"
            >
                <DialogContent>
                    <PDFViewer width='100%' height='500px'>
                        <Document>
                            <Page style={styles.body}>
                                <Text style={styles.header}>~ Created with react-pdf ~</Text>
                                <Text style={styles.title}>Boleta</Text>
                                <Text style={styles.author}>Restaurante Seven</Text>
                                <Text style={styles.subTitle}>Cliente</Text>
                                <Text
                                    style={
                                        styles.text
                                    }>{pedido.dniCliente}</Text>
                                <Text style={styles.subTitle}>Detalle Boleta</Text>
                                <Text style={styles.text}> Codigo Boleta: {pedido.id}</Text>
                                <Text style={styles.subTitle}>Productos</Text>
                                {pedido.items && pedido.items.map((p) => (
                                    <>
                                        <Text style={styles.textItem}>
                                            Producto Desc.: {p.producto.nombre}
                                        </Text>
                                        <Text style={styles.textItem}>
                                            Cantidad: {p.cantidad}
                                        </Text>
                                        <Text style={styles.textItem}>
                                            Precio Un.: S/{p.producto.precio}
                                        </Text>
                                        <Text style={styles.textItem}>
                                            SubTotal: S/{p.producto.precio * p.cantidad}
                                        </Text>
                                        <Text style={styles.textItem}>
                                            -------------------
                                        </Text>
                                    </>
                                ))}
                                <Text style={styles.price}>Cuenta Total: S/50</Text>
                            </Page>
                        </Document>
                    </PDFViewer>
                </DialogContent>
            </Dialog>

            <Dialog fullWidth={true}
                    maxWidth='xs'
                    open={openModalConfirmation}
                    onClose={hableCloseModalConfirmation}
                    aria-labelledby="max-width-dialog-title">
                <DialogTitle id="alert-dialog-title">{"Esta seguro que desea cerrar este pedido?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Asegurese que lo cobrado sea lo correcto y
                        revisar bien las monedas y billetes antes de dar por
                        concluido el pago

                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={hableCloseModalConfirmation} color="primary">
                        cancelar
                    </Button>
                    <Button onClick={() => pagarPedido()} color="primary" autoFocus>
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        </>
    )
        ;
}


export default TablePedido