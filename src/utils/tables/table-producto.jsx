import {
    IconButton, Paper, Snackbar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow
} from "@material-ui/core";
import {Block, Check, Delete, Edit} from "@material-ui/icons";
import React, {useState, useEffect} from "react";
import ModalProducto from "../modals/producto-modal";
import CloseIcon from "@material-ui/icons/Close";
import {useSelector} from "react-redux";

function TableProducto({columns, data}) {
    const state = useSelector((state) => state.producto);
    const [lista] = useState({...data})
    const [openModal, setOpenModal] = useState(false)
    const [dataProducto, setUserData] = useState()
    const [rowsPerPage, setRowsPerPage] = React.useState(10)
    const [page, setPage] = React.useState(0)

    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    useEffect(() => {
        setSnackBarStatus(state.list.snackBar);
        setSnackBarMessage(state.list.message)
    }, [state])
    const [snackBarStatus, setSnackBarStatus] = useState(false)
    const [snackBarMessage, setSnackBarMessage] = useState()
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    const onEdit = (data) => {
        const newForm = {
            id: data.id,
            nombre: data.nombre,
            estado: data.estado,
            categoria: data.categoria,
            precio: data.precio,
            cantidad: data.cantidad,
            fechaVencimiento: data.fechaVencimiento,
        }
        setUserData(newForm)
        setOpenModal(true)
    }
    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }
        setSnackBarStatus(false);
    };
    return (
        <>
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    key={'categoria'}
                                    style={{minWidth: '170'}}
                                >
                                    Categoria
                                </TableCell>
                                <TableCell
                                    key={'nombre'}
                                    style={{minWidth: '170'}}
                                >
                                    Nombre
                                </TableCell>



                                <TableCell
                                    key={'precio'}
                                    style={{minWidth: '170'}}
                                >
                                    Precio
                                </TableCell>

                                <TableCell
                                    key={'cantidad'}
                                    style={{minWidth: '170'}}
                                >
                                    Cantidad
                                </TableCell>

                                <TableCell
                                    key={'estado'}
                                    style={{minWidth: '170'}}
                                >
                                    Estado
                                </TableCell>

                                <TableCell
                                    key={'accion'}
                                    style={{minWidth: '170'}}
                                >
                                    Accion
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {lista.list.data.map((row) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}
                                              style={{background: !row.estado ? '#AAB7B8' : ''}}>
                                        <TableCell>
                                            {row.categoria.nombre}
                                        </TableCell>
                                        <TableCell>
                                            {row.nombre}
                                        </TableCell>

                                        <TableCell>
                                            {row.precio}
                                        </TableCell>
                                        <TableCell>
                                            {row.cantidad}
                                        </TableCell>
                                        <TableCell>
                                            {row.estado ? <Check/> : <Block/>}
                                        </TableCell>
                                        <TableCell>
                                            <IconButton color="primary" edge="end" aria-label="edit"
                                                        onClick={() => onEdit(row)}>
                                                <Edit/>
                                            </IconButton>
                                            <IconButton color="secondary" edge="end" aria-label="delete">
                                                <Delete/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={lista.list.data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            <ModalProducto openModal={openModal} data={dataProducto}/>
            <Snackbar open={snackBarStatus} autoHideDuration={7000} onClose={handleClose}
                      message={<span id="snackbar-fab-message-id"> {snackBarMessage} </span>}
                      action={[
                          <IconButton key="close" aria-label="Close" color="inherit" onClick={handleClose}>
                              <CloseIcon/>
                          </IconButton>,
                      ]}/>
        </>
    );
}

export default TableProducto