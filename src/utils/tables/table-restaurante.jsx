import {
    Avatar, Button, Chip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Icon,
    IconButton,
    Paper, Snackbar,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead, TablePagination,
    TableRow
} from "@material-ui/core";
import {Block, Check, Delete, Edit} from "@material-ui/icons";
import React, {useState, useEffect} from "react";
import * as userActions from "../../redux/users/actions";
import CloseIcon from '@material-ui/icons/Close';
import {useDispatch, useSelector} from "react-redux";
import ModalRestaurante from "../modals/restaurante-modal";

function TableRestaurante({data}) {
    const state = useSelector((state) => state.restaurante);
    const [listRestaurante] = useState({...data})
    const [openModal, setOpenModal] = useState(false)
    const [openStatusDialog, setOpenStatusDialog] = React.useState(false);
    const [userData, setUserData] = useState()
    const [userId, setUserId] = useState()
    const [snackBarStatus, setSnackBarStatus] = useState(false)
    const [snackBarMessage, setSnackBarMessage] = useState()
    const [page, setPage] = React.useState(0);
    const [rowsPerPage, setRowsPerPage] = React.useState(10);
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const dispatch = useDispatch();
    const handleClickOpenStatusDialog = (row) => {
        setUserId(row.id)
        setOpenStatusDialog(true);
    };

    const handleClickCloseStatusDialog = () => {
        setOpenStatusDialog(false);
    };
    const changeUserStatus = () => {
        dispatch(userActions.changeStatus(userId, listRestaurante.list.data));
        setOpenStatusDialog(false);
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(+event.target.value);
        setPage(0);
    };
    useEffect(() => {
        setSnackBarStatus(state.list.snackBar);
        setSnackBarMessage(state.list.message)
    }, [state])
    const onEdit = (data) => {
        const newForm = {
            id: data.id,
            mail: data.mail,
            dni: data.dni,
            nombre: data.nombre,
            apellidoPaterno: data.apellidoPaterno,
            apellidoMaterno: data.apellidoMaterno,
            foto: '',
            roles: data.roles
        }
        setUserData(newForm)
        setOpenModal(true)
    }

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setSnackBarStatus(false);
    };
    return (
        <>
            <Paper>
                <TableContainer>
                    <Table stickyHeader aria-label="sticky table">
                        <TableHead>
                            <TableRow>
                                <TableCell
                                    key={'logo'}
                                    style={{minWidth: '170'}}
                                >
                                    Logo
                                </TableCell>
                                <TableCell
                                    key={'ruc'}
                                    style={{minWidth: '170'}}
                                >
                                    RUC
                                </TableCell>

                                <TableCell
                                    key={'nombre'}
                                    style={{minWidth: '170'}}
                                >
                                    Nombre
                                </TableCell>

                                <TableCell
                                    key={'codigo'}
                                    style={{minWidth: '170'}}
                                >
                                    Codigo
                                </TableCell>

                                <TableCell
                                    key={'aforo'}
                                    style={{minWidth: '170'}}
                                >
                                    Aforo
                                </TableCell>


                                <TableCell
                                    key={'estado'}
                                    style={{minWidth: '170'}}
                                >
                                    Estado
                                </TableCell>

                                <TableCell
                                    key={'accion'}
                                    style={{minWidth: '170'}}
                                >
                                    Accion
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {listRestaurante.list.data.map((row) => {
                                return (
                                    <TableRow hover role="checkbox" tabIndex={-1} key={row.id}
                                              style={{background: !row.estado ? '#AAB7B8' : ''}}>
                                        <TableCell>
                                            <Avatar src={row.urlFoto || ''} />
                                        </TableCell>
                                        <TableCell>
                                            {row.ruc}
                                        </TableCell>
                                        <TableCell>
                                            {row.nombre}
                                        </TableCell>
                                        <TableCell>
                                            {row.codigo}
                                        </TableCell>
                                        <TableCell>
                                            {row.aforo}
                                        </TableCell>
                                        <TableCell>
                                            {row.estado ? <IconButton
                                                onClick={() => handleClickOpenStatusDialog(row)}>
                                                <Check/>
                                            </IconButton> : <IconButton
                                                onClick={() => handleClickOpenStatusDialog(row)}>
                                                <Block/>
                                            </IconButton>}
                                        </TableCell>
                                        <TableCell>
                                            <IconButton color="primary" edge="end" aria-label="edit"
                                                        onClick={() => onEdit(row)}>
                                                <Edit/>
                                            </IconButton>
                                            <IconButton color="secondary" edge="end" aria-label="delete">
                                                <Delete/>
                                            </IconButton>
                                        </TableCell>
                                    </TableRow>
                                );
                            })}
                        </TableBody>
                    </Table>
                </TableContainer>
                <TablePagination
                    rowsPerPageOptions={[10, 25, 100]}
                    component="div"
                    count={listRestaurante.list.data.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleChangePage}
                    onChangeRowsPerPage={handleChangeRowsPerPage}
                />
            </Paper>
            <ModalRestaurante openModal={openModal} data={userData}/>
            <Dialog
                open={openStatusDialog}
                onClose={handleClickCloseStatusDialog}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">ALERTA!</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Esta seguro que desea cambiar el estado ?
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={changeUserStatus} color="primary">
                        Si
                    </Button>
                    <Button onClick={handleClickCloseStatusDialog} color="primary" autoFocus>
                        No
                    </Button>
                </DialogActions>
            </Dialog>

            <Snackbar
                open={snackBarStatus}
                autoHideDuration={7000}
                onClose={handleClose}

                message={<span id="snackbar-fab-message-id">
                    {snackBarMessage}
                </span>}
                action={[
                    <IconButton
                        key="close"
                        aria-label="Close"
                        color="inherit"
                        onClick={handleClose}
                    >
                        <CloseIcon/>
                    </IconButton>,
                ]}
            />
        </>
    );
}

export default TableRestaurante