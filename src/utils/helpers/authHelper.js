import React from 'react';
import {Route, Redirect, useHistory} from 'react-router-dom';
import {getCurrentUser} from './Utils';
import Cookies from "js-cookie";
import {TOKEN_NAME, USER_COOKIE} from "../constants/defaultValues";

const ProtectedRoute = ({component: Component, ...rest}) => {
    const setComponent = (props) => {
        const currentUser = Cookies.get(TOKEN_NAME);

        if (currentUser) {
            return <Component/>;
        }
        return <Redirect
            to={{
                pathname: '/',
                state: {from: props.location},
            }}
        />;
    };

    return <Route render={setComponent}/>;
};

// eslint-disable-next-line import/prefer-default-export
export {ProtectedRoute};
