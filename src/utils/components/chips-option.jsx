import React, {useEffect, useState} from 'react';
import {Multiselect} from 'multiselect-react-dropdown';
import {useDispatch, useSelector} from "react-redux";
import * as rolesActions from "../../redux/roles/action";

export default function ChipsArray() {
    const state = useSelector((state) => state.roles);
    const dispatch = useDispatch();
    const [chipData, setChipData] = React.useState([
        {key: 0, label: 'Angular'},
        {key: 1, label: 'jQuery'},
        {key: 2, label: 'Polymer'},
        {key: 3, label: 'React'},
        {key: 4, label: 'Vue.js'},
    ]);
    const getRoles = () => {
        dispatch(rolesActions.getRoles());
    }
    useEffect(() => {
        getRoles();
    }, [])
    const handleChange = (event) => {
        console.log(event);
    };

    const handleDelete = (chipToDelete) => () => {
        setChipData((chips) => chips.filter((chip) => chip.key !== chipToDelete.key));
    };
    let dataChip = state.list.data;
    console.log(dataChip)
    return (
        <Multiselect
            options={chipData} // Options to display in the dropdown
            selectedValues={chipData} // Preselected value to persist in dropdown
            onSelect={handleChange} // Function will trigger on select event
            onRemove={handleDelete} // Function will trigger on remove event
            displayValue="nombre" // Property name to display in the dropdown options
        />

    );
}
