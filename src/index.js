/* eslint-disable global-require */
import './styles/assets/css/vendor/bootstrap.min.css';
import './styles/assets/css/vendor/bootstrap.rtl.only.min.css';
import 'react-circular-progressbar/dist/styles.css';
import 'react-perfect-scrollbar/dist/css/styles.css';
import 'react-big-calendar/lib/css/react-big-calendar.css';
import 'react-image-lightbox/style.css';

const render = () => {
    import(`./styles/assets/css/sass/themes/gogo.light.purplemonster.scss`).then(() => {
        require('./App');
    });
};
render();