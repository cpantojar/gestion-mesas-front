/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable no-use-before-define */
import {injectIntl} from 'react-intl';

import {
    UncontrolledDropdown,
    DropdownItem,
    DropdownToggle,
    DropdownMenu,
} from 'reactstrap';

import {NavLink} from 'react-router-dom';
import {connect, useDispatch, useSelector} from 'react-redux';

import {
    setContainerClassnames,
    clickOnMobileMenu,
    logoutUser,
    changeLocale,
} from '../../../redux/types/general_types';

import {
    adminRoot,
} from '../../../utils/constants/defaultValues';

import {MobileMenuIcon, MenuIcon} from '../../svg';
import {useEffect} from "react";
import {getCurrentUser} from "../../../utils/helpers/Utils";
import * as restauranteActions from "../../../redux/restaurante/action";


const TopNav = ({
                    intl,
                    history,
                    containerClassnames,
                    menuClickCount,
                    selectedMenuHasSubItems,
                    setContainerClassnamesAction,
                    clickOnMobileMenuAction,
                    logoutUserAction,
                }) => {

    const handleLogout = () => {
        const values = {
            history: history
        };
        logoutUserAction(values);
    };
    const dispatch = useDispatch();
    const state = useSelector((state) => state.restaurante);
    const currentUser = getCurrentUser();

    const getRestaurante = () => {
        dispatch(restauranteActions.getRestauranteById(getCurrentUser().restaurante.id));
    }

    useEffect(() => {
        getRestaurante();
    }, [])

    const menuButtonClick = (e, _clickCount, _conClassnames) => {
        e.preventDefault();

        setTimeout(() => {
            const event = document.createEvent('HTMLEvents');
            event.initEvent('resize', false, false);
            window.dispatchEvent(event);
        }, 350);
        setContainerClassnamesAction(
            _clickCount + 1,
            _conClassnames,
            selectedMenuHasSubItems
        );
    };

    const mobileMenuButtonClick = (e, _containerClassnames) => {
        e.preventDefault();
        clickOnMobileMenuAction(_containerClassnames);
    };


    return (
        <nav className="navbar fixed-top">
            <div className="d-flex align-items-center navbar-left">
                <NavLink
                    to="#"
                    location={{}}
                    className="menu-button d-none d-md-block"
                    onClick={(e) =>
                        menuButtonClick(e, menuClickCount, containerClassnames)
                    }
                >
                    <MenuIcon/>
                </NavLink>
                <NavLink
                    to="#"
                    location={{}}
                    className="menu-button-mobile d-xs-block d-sm-block d-md-none"
                    onClick={(e) => mobileMenuButtonClick(e, containerClassnames)}
                >
                    <MobileMenuIcon/>
                </NavLink>

            </div>
            <NavLink className="navbar-logo" to={adminRoot}>
                <span className=" d-none d-xs-block">
                    <img style={{height: '60px'}}
                         src={state.list.data ? state.list.data.urlFoto : 'https://i.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif'}></img>
                </span>
                <span className=" d-block d-xs-none">

                    <img style={{height: '40px'}}
                         src={state.list.data ? state.list.data.urlFoto : 'https://i.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif'}></img>
                </span>
            </NavLink>

            <div className="navbar-right">

                <div className="user d-inline-block">
                    <UncontrolledDropdown className="dropdown-menu-right">
                        <DropdownToggle className="p-0" color="empty">
                            <span className="name mr-1">{currentUser.nombre}</span>
                            <span>
                <img alt="Profile" src="/assets/img/profiles/l-1.jpg"/>
              </span>
                        </DropdownToggle>
                        <DropdownMenu className="mt-3" right>
                            <DropdownItem>Perfil</DropdownItem>
                            <DropdownItem divider/>
                            <DropdownItem onClick={() => handleLogout()}>
                                Sign out
                            </DropdownItem>
                        </DropdownMenu>
                    </UncontrolledDropdown>
                </div>
            </div>
        </nav>
    );
};

const mapStateToProps = ({menu, settings}) => {
    const {containerClassnames, menuClickCount, selectedMenuHasSubItems} = menu;
    const {locale} = settings;
    return {
        containerClassnames,
        menuClickCount,
        selectedMenuHasSubItems,
        locale,
    };
};
export default injectIntl(
    connect(mapStateToProps, {
        setContainerClassnamesAction: setContainerClassnames,
        clickOnMobileMenuAction: clickOnMobileMenu,
        logoutUserAction: logoutUser,
        changeLocaleAction: changeLocale,
    })(TopNav)
);
