import * as TYPE from "../types/restaurante";

const initialState = {
    list: {
        loading: false,
        data: null,
        error: {
            error: false,
            message: ''
        }
    },
}

export default function restaurante(state = initialState, action) {
    switch (action.type) {
        //Obtener usuario por ID
        case TYPE.GET_RESTAURANTE_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_RESTAURANTE_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_RESTAURANTE_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_RESTAURANTE_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }

            //Obtener lista de restaurantes
        case TYPE.GET_RESTAURANTES_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_RESTAURANTES_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_RESTAURANTES_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_RESTAURANTES_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
    }
    return state;
}