import * as TYPE from "../types/restaurante";
import {ENDPOINT_RESTAURANTE, ENDPOINT_USER} from "../../utils/api/enpoints";
import baseApi from "../../utils/api/baseApi";
import {getCurrentUser} from "../../utils/helpers/Utils";

export const getRestauranteById = (idRestaurante) => async (dispatch) => {
    dispatch({type: TYPE.GET_RESTAURANTE_START})
    try {
        const urlListUser = `${ENDPOINT_RESTAURANTE}${idRestaurante}`
        const response = await baseApi.get(urlListUser)
        dispatch({
            type: TYPE.GET_RESTAURANTE_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_RESTAURANTE_FAIL,
            error: {
                error: true,
                message: 'Error Obtener Restaurante'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_RESTAURANTE_FINISH})
    }
}

export const getRestaurantes = () => async (dispatch) => {
    dispatch({type: TYPE.GET_RESTAURANTES_START})
    try {
        const urlListUser = `${ENDPOINT_RESTAURANTE}listar`
        const response = await baseApi.get(urlListUser)
        dispatch({
            type: TYPE.GET_RESTAURANTES_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_RESTAURANTES_FAIL,
            error: {
                error: true,
                message: 'Error al listar restaurantes'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_RESTAURANTES_FINISH})
    }
}