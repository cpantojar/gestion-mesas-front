import {combineReducers} from 'redux';
import settings from './settings/reducer';
import menu from './menu/reducer';
import authUser from './auth/reducer';
import users from "./users/reducer";
import roles from "./roles/reducer"
import categoria from "./categoria/reducer"
import producto from "./producto/reducer"
import restaurante from "./restaurante/reducer"
import mesas from "./mesa/reducer"
import pedidosReducer from "./pedido/reducer"
const reducers = combineReducers({
    menu,
    settings,
    authUser,
    users,
    roles,
    categoria,
    producto,
    restaurante,
    mesas,
    pedidosReducer
});

export default reducers;
