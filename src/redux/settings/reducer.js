import { CHANGE_LOCALE } from '../types/general_types';
import { getCurrentLanguage } from '../../utils/helpers/Utils';

const INIT_STATE = {
  locale: getCurrentLanguage(),
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case CHANGE_LOCALE:
      return { ...state, locale: action.payload };

    default:
      return { ...state };
  }
};
