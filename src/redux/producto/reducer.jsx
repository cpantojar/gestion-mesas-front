import * as TYPE from "../types/producto";

const initialState = {
    list: {
        loading: false,
        data: null,
        error: {
            error: false,
            message: ''
        }
    },
}
export default function productos(state = initialState, action) {
    switch (action.type) {
        case TYPE.GET_PRODUCTOS_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_PRODUCTOS_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_PRODUCTOS_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_PRODUCTOS_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }

        //add new producto
        case TYPE.ADD_PRODUCTO_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.ADD_PRODUCTO_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    snackBar: true,
                    modal: false,
                    message: "el producto se creo correctamente"
                },
            }
        case TYPE.ADD_PRODUCTO_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.ADD_PRODUCTO_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
    }
    return state;
}