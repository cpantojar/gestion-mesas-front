import * as TYPE from "../types/producto";
import {ENDPOINT_PRODUCTO} from "../../utils/api/enpoints";
import baseApi from "../../utils/api/baseApi";
import {getCurrentUser} from "../../utils/helpers/Utils";

export const getProductos = () => async (dispatch) => {
    const restauranteCode = getCurrentUser().restaurante.codigo;
    dispatch({type: TYPE.GET_PRODUCTOS_START})
    try {
        const urlListProducto = `${ENDPOINT_PRODUCTO}${restauranteCode}/listar`
        const response = await baseApi.get(urlListProducto)
        dispatch({
            type: TYPE.GET_PRODUCTOS_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_PRODUCTOS_FAIL,
            error: {
                error: true,
                message: 'Error al listar productos'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_PRODUCTOS_FINISH})
    }
}

export const addProducto = (dataForm, productos) => async (dispatch) => {
    dispatch({type: TYPE.ADD_PRODUCTO_START})
    try {
        const response = await baseApi.post(`${ENDPOINT_PRODUCTO}registrar`, dataForm)
        const newListProductoUpdated = productos
        newListProductoUpdated.push(response.data)
        dispatch({
            type: TYPE.ADD_PRODUCTO_SUCCESS,
            data: newListProductoUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.ADD_PRODUCTO_FAIL,
            error: {
                error: true,
                message: 'Error al agregar Producto'
            }
        })
    } finally {
        dispatch({type: TYPE.ADD_PRODUCTO_FINISH})
    }
}

