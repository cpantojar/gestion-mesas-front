import * as TYPE from "../types/pedido";

const initialState = {
    list: {
        loading: false,
        data: null,
        error: {
            error: false,
            message: ''
        }
    },
}
export default function pedidosReducer(state = initialState, action) {
    switch (action.type) {
        // get pedidos
        case TYPE.GET_PEDIDOS_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_PEDIDOS_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_PEDIDOS_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_PEDIDOS_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }

        // get pedido by mesa
        case TYPE.GET_PEDIDO_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_PEDIDO_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_PEDIDO_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_PEDIDO_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //add new pedido
        case TYPE.ADD_PEDIDO_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.ADD_PEDIDO_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    message: "El Pedido se registro correctamente"
                },
            }
        case TYPE.ADD_PEDIDO_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error,
                    message: "El Pedido tuvo problemas al momento de registrar, porfavor verificar los campos"
                },
            }
        case TYPE.ADD_PEDIDO_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //edit pedido
        case TYPE.EDIT_PEDIDO_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.EDIT_PEDIDO_SUCCESS:
            return {
                ...state,
                response: {
                    ...state.list,
                    data: action.data
                },
            }
        case TYPE.EDIT_PEDIDO_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.EDIT_PEDIDO_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        // pagar pedido
        case TYPE.PAY_PEDIDO_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.PAY_PEDIDO_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data
                },
            }
        case TYPE.PAY_PEDIDO_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.PAY_PEDIDO_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //end pagar pedido
        // pagar pedido
        case TYPE.DELIVERED_PEDIDO_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.DELIVERED_PEDIDO_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data
                },
            }
        case TYPE.DELIVERED_PEDIDO_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.DELIVERED_PEDIDO_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //end pagar pedido
    }
    return state;
}