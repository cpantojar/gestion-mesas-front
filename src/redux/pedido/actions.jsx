import * as TYPE from "../types/pedido";
import baseApi from "../../utils/api/baseApi";
import {ENDPOINT_PEDIDO} from "../../utils/api/enpoints";
import {getCurrentUser} from "../../utils/helpers/Utils";

export const getPedidos = () => async (dispatch) => {
    const restauranteCode = getCurrentUser().restaurante.codigo;
    dispatch({type: TYPE.GET_PEDIDOS_START})
    try {
        const urlListPedidos = `${ENDPOINT_PEDIDO}${restauranteCode}/listar`
        const response = await baseApi.get(urlListPedidos)
        dispatch({
            type: TYPE.GET_PEDIDOS_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_PEDIDOS_FAIL,
            error: {
                error: true,
                message: 'Error al listar pedidos'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_PEDIDOS_FINISH})
    }
}

export const getPedidoByMesaId = (mesaId) => async (dispatch) => {
    dispatch({type: TYPE.GET_PEDIDO_START})
    try {
        const response = await baseApi.get(`${ENDPOINT_PEDIDO}get-by-mesa/${mesaId}`)
        dispatch({
            type: TYPE.GET_PEDIDO_SUCCESS,
            data: response.data
        })
    } catch (error) {
        dispatch({
            type: TYPE.GET_PEDIDO_FAIL,
            error: {
                error: true,
                message: 'Error al obtener PEDIDO!!!!'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_PEDIDO_FINISH})
    }
}

export const savePedido = (dataForm) => async (dispatch) => {
    dispatch({type: TYPE.ADD_PEDIDO_START})
    const restaurantCode = getCurrentUser().restaurante.codigo
    try {
        const response = await baseApi.post(`${ENDPOINT_PEDIDO}${restaurantCode}/registrar`, dataForm)
        //const newListPedidoUpdated = pedidos
        //newListPedidoUpdated.push(response.data)
        dispatch({
            type: TYPE.ADD_PEDIDO_SUCCESS,
            data: response.data
        })
    } catch (error) {
        dispatch({
            type: TYPE.ADD_PEDIDO_FAIL,
            error: {
                error: true,
                message: 'Error al agregar PEDIDO!!!!'
            }
        })
    } finally {
        dispatch({type: TYPE.ADD_PEDIDO_FINISH})
    }
}

export const editPedido = (dataForm) => async (dispatch) => {
    dispatch({type: TYPE.EDIT_PEDIDO_START})
    try {
        const response = await baseApi.put(`${ENDPOINT_PEDIDO}modificar/${dataForm.id}`, dataForm)
        dispatch({
            type: TYPE.EDIT_PEDIDO_SUCCESS,
            data: response.data
        })
    } catch (error) {
        dispatch({
            type: TYPE.EDIT_PEDIDO_FAIL,
            error: {
                error: true,
                message: 'Error al editar PEDIDO'
            }
        })
    } finally {
        dispatch({type: TYPE.EDIT_PEDIDO_FINISH})
    }
}

export const payPedido = (idpedido, pedidos) => async (dispatch) => {
    debugger;
    dispatch({type: TYPE.PAY_PEDIDO_START})
    try {
        const response = await baseApi.get(`${ENDPOINT_PEDIDO}cambiar_estado/${idpedido}`)
        const newListPedidoUpdated = pedidos.list.data
        const index = newListPedidoUpdated.findIndex((pedido) => pedido.id === idpedido)
        newListPedidoUpdated[index] = response.data
        dispatch({
            type: TYPE.PAY_PEDIDO_SUCCESS,
            data: newListPedidoUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.PAY_PEDIDO_FAIL,
            error: {
                error: true,
                message: 'Error al pagar PEDIDO!!!'
            }
        })
    } finally {
        dispatch({type: TYPE.PAY_PEDIDO_FINISH})
    }
}

export const entregarPedido = (idpedido, pedidos) => async (dispatch) => {
    debugger;
    dispatch({type: TYPE.DELIVERED_PEDIDO_START})
    try {
        const response = await baseApi.get(`${ENDPOINT_PEDIDO}cambiar_entregado/${idpedido}`)
        const newListPedidoUpdated = pedidos.list.data
        const index = newListPedidoUpdated.findIndex((pedido) => pedido.id === idpedido)
        newListPedidoUpdated[index] = response.data
        dispatch({
            type: TYPE.DELIVERED_PEDIDO_SUCCESS,
            data: newListPedidoUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.DELIVERED_PEDIDO_FAIL,
            error: {
                error: true,
                message: 'Error al pagar PEDIDO!!!'
            }
        })
    } finally {
        dispatch({type: TYPE.DELIVERED_PEDIDO_FINISH})
    }
}