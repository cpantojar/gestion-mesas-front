import * as TYPE from "../types/mesa";

const initialState = {
    list: {
        loading: false,
        data: null,
        error: {
            error: false,
            message: ''
        }
    },
}

export default function mesas(state = initialState, action) {
    switch (action.type) {
        case TYPE.GET_MESAS_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_MESAS_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    modal: false,
                    message: "la mesa se creo correctamente"
                },
            }
        case TYPE.GET_MESAS_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_MESAS_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }

        //add new  mesa
        case TYPE.ADD_MESA_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.ADD_MESA_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data
                },
            }
        case TYPE.ADD_MESA_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.ADD_MESA_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }

        //editar mesa
        case TYPE.EDIT_MESA_START :
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.EDIT_MESA_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    modal: false,
                    message: "la mesa se edito correctamente"
                },
            }
        case TYPE.EDIT_MESA_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.EDIT_MESA_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
    }
    return state;
}