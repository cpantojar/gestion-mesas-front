import * as TYPE from "../types/mesa";
import {ENDPOINT_MESA} from "../../utils/api/enpoints";
import baseApi from "../../utils/api/baseApi";
import {getCurrentUser} from "../../utils/helpers/Utils";

export const getMesas = () => async (dispatch) => {
    const restauranteCode = getCurrentUser().restaurante.codigo;
    dispatch({type: TYPE.GET_MESAS_START})
    try {
        const urlListMesas = `${ENDPOINT_MESA}${restauranteCode}/listar`
        const response = await baseApi.get(urlListMesas)
        dispatch({
            type: TYPE.GET_MESAS_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_MESAS_FAIL,
            error: {
                error: true,
                message: 'Error al listar mesas'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_MESAS_FINISH})
    }
}

export const addMesas = (dataForm, mesas) => async (dispatch) => {
    dispatch({type: TYPE.ADD_MESA_START})
    const restauranteCode = getCurrentUser().restaurante.codigo
    try {
        const response = await baseApi.post(`${ENDPOINT_MESA}${restauranteCode}/registrar`, dataForm)
        const newListMesas = mesas
        newListMesas.push(response.data)
        dispatch({
            type: TYPE.ADD_MESA_SUCCESS,
            data: newListMesas
        })
    } catch (error) {
        dispatch({
            type: TYPE.ADD_MESA_FAIL,
            error: {
                error: true,
                message: 'Error al agregar Mesa'
            }
        })
    } finally {
        dispatch({type: TYPE.ADD_MESA_FINISH})
    }
}


export const editMesa = (dataForm, mesas) => async (dispatch) => {
    dispatch({type: TYPE.EDIT_MESA_START})
    try {
        const response = await baseApi.put(`${ENDPOINT_MESA}modificar/${dataForm.id}`, dataForm)
        const newListMesas = mesas
        const index = newListMesas.findIndex((user) => user.id === dataForm.id)
        newListMesas[index] = response.data
        dispatch({
            type: TYPE.EDIT_MESA_SUCCESS,
            data: newListMesas
        })
    } catch (error) {
        dispatch({
            type: TYPE.EDIT_MESA_FAIL,
            error: {
                error: true,
                message: 'Error al editar Mesa'
            }
        })
    } finally {
        dispatch({type: TYPE.EDIT_MESA_FINISH})
    }
}