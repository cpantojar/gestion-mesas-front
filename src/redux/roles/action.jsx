import * as TYPE from "../types/roles";
import {ENDPOINT_ROLES} from "../../utils/api/enpoints";
import baseApi from "../../utils/api/baseApi";

export const getRoles = () => async (dispatch) => {
    dispatch({type: TYPE.GET_ROLES_START})
    try {
        const urlListRoles = `${ENDPOINT_ROLES}listar`
        const response = await baseApi.get(urlListRoles)
        dispatch({
            type: TYPE.GET_ROLES_SUCCESS,
            data: response.data
        })
    } catch (error) {
        dispatch({
            type: TYPE.GET_ROLES_FAIL,
            error: {
                error: true,
                message: 'Error en listar roles'
            }
        })

    } finally {
        dispatch({type: TYPE.GET_ROLES_FINISH})
    }
}