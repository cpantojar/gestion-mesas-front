import * as TYPE from '../types/roles'

const initialState = {
    list: {
        loading: false,
        data: null,
        error: {
            error: false,
            message: ''
        }
    },
}
export default function roles(state = initialState, action) {
    switch (action.type) {
        case TYPE.GET_ROLES_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_ROLES_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_ROLES_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_ROLES_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
    }
    return state;
}