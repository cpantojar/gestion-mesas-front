import * as TYPE from '../types/users'
import baseApi from "../../utils/api/baseApi";
import {ENDPOINT_USER} from "../../utils/api/enpoints";
import {getCurrentUser} from "../../utils/helpers/Utils";

export const getUsers = () => async (dispatch) => {
    dispatch({type: TYPE.GET_USERS_START})
    try {
        const restauranteCode = getCurrentUser().restaurante.codigo
        const urlListUser = `${ENDPOINT_USER}${restauranteCode}/listar`
        const response = await baseApi.get(urlListUser)
        dispatch({
            type: TYPE.GET_USERS_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_USERS_FAIL,
            error: {
                error: true,
                message: 'Error al listar usuarios'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_USERS_FINISH})
    }
}

export const addNewUser = (dataForm, users) => async (dispatch) => {
    dispatch({ type: TYPE.ADD_USER_START })
    try {
        const restauranteCode = getCurrentUser().restaurante.codigo
        const response = await baseApi.post(`${ENDPOINT_USER}${restauranteCode}/registrar`, dataForm)
        const newListUsersUpdated = users
        newListUsersUpdated.push(response.data)
        dispatch({
            type: TYPE.ADD_USER_SUCCESS,
            data: newListUsersUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.ADD_USER_FAIL,
            error: {
                error: true,
                message: 'Error al agregar usuario'
            }
        })
    } finally {
        dispatch({ type: TYPE.ADD_USER_FINISH })
    }
}

export const changeStatus = (idUsuario, users) => async (dispatch) => {
    dispatch({ type: TYPE.CHANGE_STATUS_USER_START })
    try {
        const response = await baseApi.get(`${ENDPOINT_USER}cambiar_estado/${idUsuario}`)
        const newListUsersUpdated = users
        const index = newListUsersUpdated.findIndex((user) => user.id === idUsuario)
        newListUsersUpdated[index] = response.data
        dispatch({
            type: TYPE.CHANGE_STATUS_USER_SUCCESS,
            data: newListUsersUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.CHANGE_STATUS_USER_FAIL,
            error: {
                error: true,
                message: 'Error al cambiar estado de Usuario'
            }
        })
    } finally {
        dispatch({ type: TYPE.CHANGE_STATUS_USER_FINISH })
    }
}

export const editUser = (dataForm, users) => async (dispatch) => {
    dispatch({ type: TYPE.EDIT_USER_START })
    try {
        const response = await baseApi.post(`${ENDPOINT_USER}editar/${dataForm.id}`, dataForm)
        const newListUsersUpdated = users
        const index = newListUsersUpdated.findIndex((user) => user.id === dataForm.id)
        newListUsersUpdated[index] = response.data
        dispatch({
            type: TYPE.EDIT_USER_SUCCESS,
            data: newListUsersUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.EDIT_USER_FAIL,
            error: {
                error: true,
                message: 'Error al actualizar usuario'
            }
        })
    } finally {
        dispatch({ type: TYPE.EDIT_USER_FINISH })
    }
}