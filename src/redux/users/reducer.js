import * as TYPE from '../types/users'

const initialState = {
    list: {
        loading: false,
        data: null,
        modal: false,
        error: {
            error: false,
            message: ''
        }
    },
}

export default function users(state = initialState, action) {
    switch (action.type) {
        case TYPE.GET_USERS_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_USERS_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_USERS_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_USERS_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //AGREGAR USUARIO
        case TYPE.ADD_USER_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.ADD_USER_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    modal: false,
                    message: "El usuario se creo correctamente"
                },
            }
        case TYPE.ADD_USER_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.ADD_USER_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //cambiar estado de usuario
        case TYPE.CHANGE_STATUS_USER_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        case TYPE.CHANGE_STATUS_USER_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    message: "El estado del usuario se cambio correctamente"
                },
            }
        case TYPE.CHANGE_STATUS_USER_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        case TYPE.CHANGE_STATUS_USER_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //editar usuario
        case TYPE.EDIT_USER_START :
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.EDIT_USER_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    modal: false,
                    message: "El usuario se edito correctamente"
                },
            }
        case TYPE.EDIT_USER_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.EDIT_USER_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
    }
    return state;
}