import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_ERROR,
    LOGOUT_USER,
} from '../types/general_types';

const INIT_STATE = {
    authenticated: false,
    loading: false,
    error: '',
};

// eslint-disable-next-line import/no-anonymous-default-export
export default function auth(state = INIT_STATE, action) {
    switch (action.type) {
        case LOGIN_USER:
            return {
                ...state,
                loading: true,
                error: ''
            };
        case LOGIN_USER_SUCCESS:
            return {
                ...state,
                loading: false,
                currentUser: action.payload,
                error: '',
                authenticated: action.isAuth,
            };
        case LOGIN_USER_ERROR:
            return {
                ...state,
                loading: false,
                currentUser: null,
                error: action.payload,
            };

        case LOGOUT_USER:
            return {
                ...state,
                currentUser: null,
                error: '',
                authenticated: action.isAuth,
            };
        default:
            return {...state};
    }
};
