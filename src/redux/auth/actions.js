// eslint-disable-next-line import/no-cycle
import {
    LOGIN_USER,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_ERROR,
} from '../types/general_types';
import {setCurrentUser} from "../../utils/helpers/Utils";
import jwt_decode from "jwt-decode";
import baseApi from "../../utils/api/baseApi";
import {removeToken, removeUser} from "../../utils/config/credentials";
import {ENDPOINT_LOGIN} from "../../utils/api/enpoints";
import {adminRoot} from "../../utils/constants/defaultValues";

export const loginUser = (payload) => async (dispatch) => {
    const history  = payload.history;
    const user = {
        mail:payload.mail,
        password:payload.password
    };

    dispatch({type: LOGIN_USER});
    try {
        const response = await baseApi.post(ENDPOINT_LOGIN, user)
        setCurrentUser(response.data.token,jwt_decode(response.data.token), response.data.expiracion);
        dispatch({
            type: LOGIN_USER_SUCCESS,
            payload: jwt_decode(response.data.token),
            isAuth: true,
        })
        history.push(adminRoot);
    } catch (error) {
        dispatch({
            type: LOGIN_USER_ERROR,
            payload: 'Credenciales incorrectas'
        })
    }
};
export const logoutUser = (values) => async (dispatch) => {
    const history  = values.history;
    removeToken();
    removeUser();
    history.push('/')
};
