import * as TYPE from "../types/categoria";

const initialState = {
    list: {
        loading: false,
        data: null,
        error: {
            error: false,
            message: ''
        }
    },
}
export default function categoria(state = initialState, action) {
    switch (action.type) {
        case TYPE.GET_CATEGORIAS_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_CATEGORIAS_SUCCESS:
            return {
                ...state,
                list: {
                    data: action.data,
                    loading: false,
                },
            }
        case TYPE.GET_CATEGORIAS_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                }
            }
        case TYPE.GET_CATEGORIAS_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }

        //AGREGAR CATEGORIA
        case TYPE.ADD_CATEGORIA_START:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.ADD_CATEGORIA_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    modal: false,
                    message: "La categoria se creo correctamente"
                },
            }
        case TYPE.ADD_CATEGORIA_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.ADD_CATEGORIA_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
        //obtener categoria por ID
        case TYPE.GET_CATEGORIA_START:
            return {
                ...state,
                categoria: {
                    ...state.categoria,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.GET_CATEGORIA_SUCCESS:
            return {
                ...state,
                categoria: {
                    ...state.categoria,
                    data: action.data
                },
            }
        case TYPE.GET_CATEGORIA_FAIL:
            return {
                ...state,
                categoria: {
                    ...state.categoria,
                    error: action.error
                },
            }
        case TYPE.GET_CATEGORIA_FINISH:
            return {
                ...state,
                categoria: {
                    ...state.categoria,
                    loading: false
                },
            }

        //editar CATEGORIA
        case TYPE.EDIT_CATEGORIA_START :
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: true,
                    error: initialState.list.error
                },
            }
        case TYPE.EDIT_CATEGORIA_SUCCESS:
            return {
                ...state,
                list: {
                    ...state.list,
                    data: action.data,
                    loading: false,
                    snackBar: true,
                    modal: false,
                    message: "La categoria se edito correctamente"
                },
            }
        case TYPE.EDIT_CATEGORIA_FAIL:
            return {
                ...state,
                list: {
                    ...state.list,
                    error: action.error
                },
            }
        case TYPE.EDIT_CATEGORIA_FINISH:
            return {
                ...state,
                list: {
                    ...state.list,
                    loading: false
                },
            }
    }
    return state;
}