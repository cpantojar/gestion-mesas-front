import * as TYPE from "../types/categoria";
import {ENDPOINT_CATEGORIA} from "../../utils/api/enpoints";
import baseApi from "../../utils/api/baseApi";
import {getCurrentUser} from "../../utils/helpers/Utils";

export const getCategoria = () => async (dispatch) => {
    const restauranteCode = getCurrentUser().restaurante.codigo;
    dispatch({type: TYPE.GET_CATEGORIAS_START})
    try {
        const urlListCategoria = `${ENDPOINT_CATEGORIA}${restauranteCode}/listar`
        const response = await baseApi.get(urlListCategoria)
        dispatch({
            type: TYPE.GET_CATEGORIAS_SUCCESS,
            data: response.data
        })

    } catch (error) {
        dispatch({
            type: TYPE.GET_CATEGORIAS_FAIL,
            error: {
                error: true,
                message: 'Error al listar CATEGORIAS'
            }
        })
    } finally {
        dispatch({type: TYPE.GET_CATEGORIAS_FINISH})
    }
}

export const addNewCategoria = (dataForm, categorias) => async (dispatch) => {
    dispatch({ type: TYPE.ADD_CATEGORIA_START })
    const restauranteCode = getCurrentUser().restaurante.codigo
    try {
        const response = await baseApi.post(`${ENDPOINT_CATEGORIA}${restauranteCode}/registrar`, dataForm)
        const newListCategoriaUpdated = categorias
        newListCategoriaUpdated.push(response.data)
        dispatch({
            type: TYPE.ADD_CATEGORIA_SUCCESS,
            data: newListCategoriaUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.ADD_CATEGORIA_FAIL,
            error: {
                error: true,
                message: 'Error al agregar Categoria'
            }
        })
    } finally {
        dispatch({ type: TYPE.ADD_CATEGORIA_FINISH })
    }
}

export const getCategoriabyId = (idCategoria) => async (dispatch) => {
    dispatch({ type: TYPE.GET_CATEGORIA_START })
    try {
        const response = await baseApi.get(`${ENDPOINT_CATEGORIA}${idCategoria}`)
        dispatch({
            type: TYPE.GET_CATEGORIA_SUCCESS,
            data: response.data
        })
    } catch (error) {
        dispatch({
            type: TYPE.GET_CATEGORIA_FAIL,
            error: {
                error: false,
                message: 'Ocurrió un error al obtener CATEGORIA'
            }
        })
    } finally {
        dispatch({ type: TYPE.GET_CATEGORIA_FINISH })
    }
}

export const editCategoria = (dataForm, categorias) => async (dispatch) => {
    dispatch({ type: TYPE.EDIT_CATEGORIA_START })
    try {
        const response = await baseApi.put(`${ENDPOINT_CATEGORIA}modificar/${dataForm.id}`, dataForm)
        const newListCategoriaUpdated = categorias
        const index = newListCategoriaUpdated.findIndex((user) => user.id === dataForm.id)
        newListCategoriaUpdated[index] = response.data
        dispatch({
            type: TYPE.EDIT_CATEGORIA_SUCCESS,
            data: newListCategoriaUpdated
        })
    } catch (error) {
        dispatch({
            type: TYPE.EDIT_CATEGORIA_FAIL,
            error: {
                error: true,
                message: 'Error al actualizar categoria'
            }
        })
    } finally {
        dispatch({ type: TYPE.EDIT_CATEGORIA_FINISH })
    }
}