import {createStore, applyMiddleware, compose} from 'redux';
import thunk from "redux-thunk";
import reducers from './reducers';

export function configureStore(initialState) {
    let composeEnhancers = compose;
    const middlewares = [thunk];
    const enhancers = [applyMiddleware(...middlewares)];
    const store = createStore(
        reducers,
        initialState,
        composeEnhancers(...enhancers),
    );

    if (module.hot) {
        module.hot.accept('./reducers', () => {
            const nextRootReducer = require('./reducers');
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}
