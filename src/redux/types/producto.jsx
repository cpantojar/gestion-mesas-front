export const GET_PRODUCTOS_START = 'GET_PRODUCTOS_START'
export const GET_PRODUCTOS_SUCCESS = 'GET_PRODUCTOS_SUCCESS'
export const GET_PRODUCTOS_FAIL = 'GET_PRODUCTOS_FAIL'
export const GET_PRODUCTOS_FINISH = 'GET_PRODUCTOS_FINISH'

export const DELETE_PRODUCTO_START = 'DELETE_PRODUCTO_START'
export const DELETE_PRODUCTO_SUCCESS = 'DELETE_PRODUCTO_SUCCESS'
export const DELETE_PRODUCTO_FAIL = 'DELETE_PRODUCTO_FAIL'
export const DELETE_PRODUCTO_FINISH = 'DELETE_PRODUCTO_FINISH'

export const ADD_PRODUCTO_START = 'ADD_PRODUCTO_START'
export const ADD_PRODUCTO_SUCCESS = 'ADD_PRODUCTO_SUCCESS'
export const ADD_PRODUCTO_FAIL = 'ADD_PRODUCTO_FAIL'
export const ADD_PRODUCTO_FINISH = 'ADD_PRODUCTO_FINISH'

export const EDIT_PRODUCTO_START = 'EDIT_PRODUCTO_START'
export const EDIT_PRODUCTO_SUCCESS = 'EDIT_PRODUCTO_SUCCESS'
export const EDIT_PRODUCTO_FAIL = 'EDIT_PRODUCTO_FAIL'
export const EDIT_PRODUCTO_FINISH = 'EDIT_PRODUCTO_FINISH'