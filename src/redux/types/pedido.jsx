export const GET_PEDIDOS_START = 'GET_PEDIDOS_START'
export const GET_PEDIDOS_SUCCESS = 'GET_PEDIDOS_SUCCESS'
export const GET_PEDIDOS_FAIL = 'GET_PEDIDOS_FAIL'
export const GET_PEDIDOS_FINISH = 'GET_PEDIDOS_FINISH'

export const PAY_PEDIDO_START = 'DELETE_PEDIDO_START'
export const PAY_PEDIDO_SUCCESS = 'DELETE_PEDIDO_SUCCESS'
export const PAY_PEDIDO_FAIL = 'DELETE_PEDIDO_FAIL'
export const PAY_PEDIDO_FINISH = 'DELETE_PEDIDO_FINISH'

export const DELIVERED_PEDIDO_START = 'DELIVERED_PEDIDO_START'
export const DELIVERED_PEDIDO_SUCCESS = 'DELIVERED_PEDIDO_SUCCESS'
export const DELIVERED_PEDIDO_FAIL = 'DELIVERED_PEDIDO_FAIL'
export const DELIVERED_PEDIDO_FINISH = 'DELIVERED_PEDIDO_FINISH'

export const ADD_PEDIDO_START = 'ADD_PEDIDO_START'
export const ADD_PEDIDO_SUCCESS = 'ADD_PEDIDO_SUCCESS'
export const ADD_PEDIDO_FAIL = 'ADD_PEDIDO_FAIL'
export const ADD_PEDIDO_FINISH = 'ADD_PEDIDO_FINISH'

export const EDIT_PEDIDO_START = 'EDIT_PEDIDO_START'
export const EDIT_PEDIDO_SUCCESS = 'EDIT_PEDIDO_SUCCESS'
export const EDIT_PEDIDO_FAIL = 'EDIT_PEDIDO_FAIL'
export const EDIT_PEDIDO_FINISH = 'EDIT_PEDIDO_FINISH'

export const GET_PEDIDO_START = 'GET_PEDIDO_START'
export const GET_PEDIDO_SUCCESS = 'GET_PEDIDO_SUCCESS'
export const GET_PEDIDO_FAIL = 'GET_PEDIDO_FAIL'
export const GET_PEDIDO_FINISH = 'GET_PEDIDO_FINISH'