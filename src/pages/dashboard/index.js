import React, {useEffect} from 'react';
import {connect, useDispatch, useSelector} from 'react-redux';
import AppLayout from '../../layout/AppLayout';
import {Row} from "reactstrap";
import {Colxx, Separator} from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import IntlMessages from "../../utils/helpers/IntlMessages";
import List from "reactstrap/es/List";
import {Avatar, Divider, ListItem, ListItemAvatar, ListItemText, Typography} from "@material-ui/core";
import * as pedidoActions from "../../redux/pedido/actions";
import {array} from "prop-types";

const Dashboard = () => {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.pedidosReducer);
    const getPedidos = () => {
        dispatch(pedidoActions.getPedidos());
    }
    useEffect(() => {
        getPedidos();
    }, [])
    const pedidosDeliveryList = () => {
        if (state.list.data !== null) {
            return state.list.data.filter((pedido) => pedido.tipo === 2).reduce((acc, rec) => {
                const productos = rec.items.reduce((result, item) => {
                    const itemsInAcc = result.filter(a => a.nombre === item.producto.nombre)
                    if (itemsInAcc.length > 0) {
                        itemsInAcc[0].duration = (+itemsInAcc[0].duration) + (+item.duration)
                    } else {
                        result = [...result, {nombre: item.producto.nombre, cantidad: item.cantidad}]
                    }
                    return result
                }, []);
                acc = acc.concat(productos)
                //acc = [...acc, productos]
                return acc;
            }, []).reduce((r, p) => {
                const itemsInAcc = r.filter(a => a.nombre === p.nombre)
                if (itemsInAcc.length > 0) {
                    itemsInAcc[0].cantidad = (+itemsInAcc[0].cantidad) + (+p.cantidad)
                } else {
                    r = [...r, {...p}]
                }
                return r
            }, [])
        }
    }
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.dashboard"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12" className="mb-4">
                            <p>
                                <IntlMessages id="menu.dashboard"/>
                            </p>
                        </Colxx>
                    </Row>

                    <Row>
                        <div className="card-body text-center">
                            <div>
                                <h5 className="mb-3 font-weight-semibold">
                                    Lista de Pedidos Delivery
                                </h5>
                            </div>
                            <div>
                                <Colxx xxs="12" className="mb-4">
                                    <List>
                                        {pedidosDeliveryList() && pedidosDeliveryList().map((row) => {
                                            return (
                                                <div>
                                                    <ListItem alignItems="flex-start">
                                                        <ListItemAvatar>
                                                            <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg"/>
                                                        </ListItemAvatar>
                                                        <ListItemText
                                                            primary={row.nombre}
                                                            secondary={
                                                                <React.Fragment>
                                                                    <Typography
                                                                        component="span"
                                                                        variant="body2"
                                                                        color="textPrimary"
                                                                    >
                                                                        {row.cantidad}
                                                                    </Typography>
                                                                </React.Fragment>
                                                            }
                                                        />
                                                    </ListItem>
                                                    <Divider variant="inset" component="li"/>
                                                </div>
                                            )
                                        })}

                                    </List>
                                </Colxx>

                            </div>
                        </div>
                    </Row>

                </>
            </div>
        </AppLayout>
    );
};


export default Dashboard;
