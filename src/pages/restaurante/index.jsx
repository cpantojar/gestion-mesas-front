import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import * as restauranteActions from "../../redux/restaurante/action";
import AppLayout from "../../layout/AppLayout";
import {Card, CardBody, CardTitle, Row} from "reactstrap";
import {Colxx, Separator} from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import {CardContent, Fab, Grid, LinearProgress} from "@material-ui/core";
import {Add} from "@material-ui/icons";
import TableRestaurante from "../../utils/tables/table-restaurante";
import ModalRestaurante from "../../utils/modals/restaurante-modal";

function Restaurantes() {
    const [openModal, setOpenModal] = useState(false)
    const dispatch = useDispatch();
    const state = useSelector((state) => state.restaurante);
    const getRestaurantes = () => {
        dispatch(restauranteActions.getRestaurantes());
    }
    useEffect(() => {
        getRestaurantes();
    }, []);

    const onOpenModalProducto = () => {
        setOpenModal(true)
    }
    const onCloseModalProducto = () => {
        setOpenModal(false)
    }
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.empresa"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de Restaurante
                                            </Grid>
                                            <Grid item xs={6} sm={4}>
                                                <Fab style={{float: 'right'}}
                                                     color="primary"
                                                     aria-label="add"
                                                     onClick={onOpenModalProducto}>
                                                    <Add/>
                                                </Fab>
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {!state.list.data && <LinearProgress />}
                                        {state.list.data && state.list.data.length > 0 ?
                                            <TableRestaurante data={state}/> :
                                            <div> no hay data</div>}
                                    </CardContent>


                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
            <ModalRestaurante openModal={openModal} onCloseModal={onCloseModalProducto}/>

        </AppLayout>

    )
        ;
}

export default Restaurantes;
