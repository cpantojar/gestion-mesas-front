import React, {useEffect, useState} from 'react'
import {Button, Card, CardBody, CardTitle, FormGroup, Label, Row} from 'reactstrap';
import {Colxx, Separator} from '../../components/common/CustomBootstrap';
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import AppLayout from "../../layout/AppLayout";
import TableUser from "../../utils/tables/table-user";
import cols from "../../utils/constants/usersHeaders"
import {Add, Search} from "@material-ui/icons";
import {
    Fab,
    Grid, LinearProgress, TextField,
} from "@material-ui/core";
import {useDispatch, useSelector} from 'react-redux'
import * as userActions from '../../redux/users/actions'
import ModalUser from "../../utils/modals/user-modal";

function Users() {
    const state = useSelector((state) => state.users);
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = useState(false)
    const [correo, setCorreo] = useState('')
    const [nombre, setNombre] = useState('')
    let headers = React.useMemo(
        () => cols, []
    );
    const getUsers = () => {
        dispatch(userActions.getUsers());
    }

    useEffect(() => {
        getUsers();
    }, [])

    const search = () => {
        console.log("entro a la busqueda")
    };

    const onOpenModalUser = () => {
        setOpenModal(true)
    }
    const onCloseModalUser = () => {
        setOpenModal(false)
    }
    const onNombreChange = (val) => {
        setNombre(val.target.value)
    }

    const onCorreoChange = (val) => {
        setCorreo(val.target.value)
    }

    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.usuarios"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>

                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        Filtros
                                    </CardTitle>

                                    <Colxx xxs="12" md="12">
                                        <Grid container spacing={2}>
                                            <Grid item xs={12} sm={3}>
                                                <FormGroup className="form-group has-float-label">
                                                    <Label>Nombre Usuario</Label>
                                                    <TextField variant="outlined"
                                                               type="text"
                                                               fullWidth
                                                               id='nombre'
                                                               value={nombre}
                                                               onChange={(val) => onNombreChange(val)}/>
                                                </FormGroup>
                                            </Grid>
                                            <Grid item xs={12} sm={2}>
                                                <FormGroup className="form-group has-float-label">
                                                    <Label>Correo</Label>
                                                    <TextField variant="outlined"
                                                               type="text"
                                                               fullWidth
                                                               id='coreo'
                                                               value={correo}
                                                               onChange={(val) => onCorreoChange(val)}/>
                                                </FormGroup>
                                            </Grid>
                                            <Grid item xs={12} sm={2}>
                                                <Button color="primary" type="submit" onClick={search}>
                                                    <Search/> Buscar
                                                </Button>
                                            </Grid>
                                        </Grid>
                                    </Colxx>
                                </CardBody>

                            </Card>
                        </Colxx>

                    </Row>
                    <br></br>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de usuarios
                                            </Grid>
                                            <Grid item xs={6} sm={4}>
                                                <Fab style={{float: 'right'}}
                                                     color="primary"
                                                     aria-label="add"
                                                     onClick={onOpenModalUser}>
                                                    <Add/>
                                                </Fab>
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    {!state.list.data && <LinearProgress />}
                                    {state.list.data ?
                                        <TableUser columns={headers} data={state}/> : <div> no hay data</div>}
                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
            <ModalUser openModal={openModal} onCloseModal={onCloseModalUser}/>
        </AppLayout>

    );
}

export default Users
