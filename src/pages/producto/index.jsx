import React, {useEffect, useState} from 'react';
import {Card, CardBody, CardTitle, Row} from 'reactstrap';
import {Colxx, Separator} from '../../components/common/CustomBootstrap';
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import {useDispatch, useSelector} from "react-redux";
import AppLayout from "../../layout/AppLayout";
import cols from "../../utils/constants/productoHeaders"
import * as productoActions from "../../redux/producto/actions";
import TableProducto from "../../utils/tables/table-producto";
import ModalProducto from "../../utils/modals/producto-modal";
import {CardContent, Fab, Grid, LinearProgress} from "@material-ui/core";
import {Add} from "@material-ui/icons";

function Productos() {
    const [openModal, setOpenModal] = useState(false)
    const dispatch = useDispatch();
    let headers = React.useMemo(
        () => cols, []
    );
    const state = useSelector((state) => state.producto);
    const getProductos = () => {
        dispatch(productoActions.getProductos());
    }
    useEffect(() => {
        getProductos();
    }, []);
    const onOpenModalProducto = () => {
        setOpenModal(true)
    }
    const onCloseModalProducto = () => {
        setOpenModal(false)
    }
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.producto"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de Productos
                                            </Grid>
                                            <Grid item xs={6} sm={4}>
                                                <Fab style={{float: 'right'}}
                                                     color="primary"
                                                     aria-label="add"
                                                     onClick={onOpenModalProducto}>
                                                    <Add/>
                                                </Fab>
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {!state.list.data && <LinearProgress />}
                                        {state.list.data && state.list.data.length > 0 ?
                                            <TableProducto columns={headers} data={state}/> :
                                            <div> no hay data</div>}
                                    </CardContent>


                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
            <ModalProducto openModal={openModal} onCloseModal={onCloseModalProducto}/>

        </AppLayout>

    )
        ;
}

export default Productos;
