import React, {useEffect, useState} from 'react';
import {withRouter} from 'react-router-dom';
import {connect, useDispatch, useSelector} from 'react-redux';

import AppLayout from '../../layout/AppLayout';
import {Card, CardBody, CardTitle, Row} from "reactstrap";
import {Colxx, Separator} from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import IntlMessages from "../../utils/helpers/IntlMessages";
import * as pedidoActions from "../../redux/pedido/actions";
import {
    Button,
    CardContent, Chip, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Fab,
    Grid, IconButton, InputLabel,
    LinearProgress, MenuItem,
    Paper, Select,
    Table, TableBody, TableCell,
    TableContainer,
    TableHead, TablePagination,
    TableRow, TextField
} from "@material-ui/core";
import TablePedido from "../../utils/tables/table-pedido";
import {Add, Block, CancelPresentation, Check, Edit} from "@material-ui/icons";
import InputSpinner from "react-bootstrap-input-spinner";
import {getCurrentUser} from "../../utils/helpers/Utils";
import * as pedidosAction from "../../redux/pedido/actions";

const Entregas = ({match}) => {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.pedidosReducer);
    const [openModal, setOpenModal] = useState(false)
    const [detallePedido, setDetallePedido] = useState([])
    const [rowsPerPage, setRowsPerPage] = React.useState(10)
    const [page, setPage] = React.useState(0)
    const [openModalConfirmation, setOpenModalConfirmation] = useState(false)
    const [idPedido, setIdPedido] = useState('')
    const getPedidos = () => {
        dispatch(pedidoActions.getPedidos());
    }
    useEffect(() => {
        getPedidos();
    }, [])
    const onOpenModal = (items) => {
        setDetallePedido(items)
        setOpenModal(true)
    }
    const pedidosDeliveryList = () => {
        if (state.list.data !== null) {
            if(getCurrentUser().roles.includes("ADMINISTRADOR")){
                return state.list.data.filter((pedido) => pedido.tipo === 2 )
            }else {
                return state.list.data.filter((x) => !x.entregado).filter((pedido) => pedido.tipo === 2 ).filter((p) => p.chofer.mail === getCurrentUser().username)
            }
        }
    }
    const getTotal = (items) => {
        return items.reduce((acc, el) => {
            return (acc += (el.cantidad * el.producto.precio))
        }, 0);
    }
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };
    const handleCloseModal = () => {
        setOpenModal(false)
    };
    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
        setPage(0);
    };
    const hableCloseModalConfirmation = () => {
        setOpenModalConfirmation(false);
    };
    const entrgarPedido = () => {
        console.log(idPedido)
        dispatch(pedidosAction.entregarPedido(idPedido, state))
        setOpenModalConfirmation(false)
    }
    const openConfirmation = (idPedido) => {
        setIdPedido(idPedido)
        setOpenModalConfirmation(true)
    }
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.delivery" match={match}/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12" className="mb-4">
                            <p>
                                <IntlMessages id="menu.delivery"/>
                            </p>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de Delivery
                                            </Grid>

                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {!pedidosDeliveryList() && <LinearProgress/>}
                                        <Paper>
                                            <TableContainer>
                                                <Table stickyHeader aria-label="sticky table">
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell key={'dniCliente'} style={{minWidth: '170'}}>
                                                                DNI Cliente
                                                            </TableCell>

                                                            <TableCell key={'mesa'} style={{minWidth: '170'}}>
                                                                Nombre Cliente
                                                            </TableCell>

                                                            <TableCell key={'direccion'} style={{minWidth: '170'}}>
                                                                Direccion
                                                            </TableCell>

                                                            <TableCell key={'referencia'} style={{minWidth: '170'}}>
                                                                Referencia
                                                            </TableCell>

                                                            <TableCell key={'productos'} style={{minWidth: '170'}}>
                                                                Productos
                                                            </TableCell>

                                                            <TableCell key={'estado'} style={{minWidth: '170'}}>
                                                                Entregado
                                                            </TableCell>

                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {pedidosDeliveryList() && pedidosDeliveryList().map((row) => {
                                                            return (
                                                                <TableRow key={row.id}>
                                                                    <TableCell>{row.dniCliente}</TableCell>
                                                                    <TableCell> {row.nombreCliente}</TableCell>
                                                                    <TableCell> {row.direccion}</TableCell>
                                                                    <TableCell> {row.referencia}</TableCell>
                                                                    <TableCell>
                                                                        <Chip
                                                                            onClick={() => onOpenModal(row.items)}
                                                                            label={row.items.length} color="primary"/>
                                                                    </TableCell>
                                                                    <TableCell>
                                                                        {row.entregado ? <Chip
                                                                            label="Entregado"
                                                                            color="primary"
                                                                            deleteIcon={<Check/>}
                                                                        /> : <IconButton onClick={() => openConfirmation(row.id)}>
                                                                            <CancelPresentation/>
                                                                        </IconButton>}
                                                                    </TableCell>

                                                                </TableRow>
                                                            );
                                                        })}
                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                            <TablePagination
                                                rowsPerPageOptions={[10, 25, 100]}
                                                component="div"
                                                count={100}
                                                rowsPerPage={rowsPerPage}
                                                page={page}
                                                onChangePage={handleChangePage}
                                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                            />
                                        </Paper>
                                    </CardContent>
                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
            <Dialog open={openModal} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    Detalle Pedido
                </DialogTitle>
                <DialogContent>
                    <TableContainer>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    <TableCell key={'cantidad'} style={{minWidth: '170'}}>
                                        Cantidad
                                    </TableCell>
                                    <TableCell key={'desc'} style={{minWidth: '170'}}>
                                        Desc
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {detallePedido.map((row) => {
                                    return (
                                        <TableRow key={row.producto.id}>
                                            <TableCell>
                                                {row.cantidad}
                                            </TableCell>
                                            <TableCell>
                                                {row.producto.nombre}
                                            </TableCell>

                                        </TableRow>
                                    );
                                })}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseModal} color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog fullWidth={true}
                    maxWidth='xs'
                    open={openModalConfirmation}
                    onClose={hableCloseModalConfirmation}
                    aria-labelledby="max-width-dialog-title">
                <DialogTitle id="alert-dialog-title">{"Asegurate de haber entragado todo el pedido"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Muchas gracias por realizar la Entrega. Buen Trabajo

                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={hableCloseModalConfirmation} color="primary">
                        cancelar
                    </Button>
                    <Button onClick={() => entrgarPedido()} color="primary" autoFocus>
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        </AppLayout>
    );
};

const mapStateToProps = ({menu}) => {
    const {containerClassnames} = menu;
    return {containerClassnames};
};

export default withRouter(connect(mapStateToProps, {})(Entregas));
