import React, {useEffect, useState} from 'react';
import {Button, Card, CardBody, CardTitle, Label, Row} from 'reactstrap';
import {Colxx, Separator} from '../../components/common/CustomBootstrap';
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import {useDispatch, useSelector} from "react-redux";
import AppLayout from "../../layout/AppLayout";
import * as mesaActions from "../../redux/mesa/actions";
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import {
    Avatar,
    CardActions,
    CardContent,
    Grid,
    IconButton,
    InputLabel,
    LinearProgress,
    makeStyles,
    MenuItem,
    Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    TextField,
} from "@material-ui/core";
import {DeckTwoTone} from "@material-ui/icons";
import {deepOrange} from "@material-ui/core/colors";
import * as categoriesAction from "../../redux/categoria/action";
import * as productosAction from "../../redux/producto/actions";
import * as pedidosAction from "../../redux/pedido/actions";
import InputSpinner from "react-bootstrap-input-spinner";
import {getCurrentUser} from "../../utils/helpers/Utils";

const DEFAULT_PEDIDO = {
    dniCliente: '',
    mesero: '',
    estado: false,
    mesa: '',
    items: []
}

function Pedidos() {
    const categoriaList = useSelector((state) => state.categoria);
    const pedidoState = useSelector((state) => state.pedidosReducer);
    const currentUser = getCurrentUser();
    const [productoList, setProductoList] = useState([]);
    const [idPedido, setIdPedido] = useState('')
    const [pedido, setPedido] = useState('')
    const [modalForm, setModalForm] = useState(DEFAULT_PEDIDO)
    const useStyles = makeStyles((theme) => ({
        root: {
            display: 'flex',
            '& > *': {
                margin: theme.spacing(1),
            },
        },
        libre: {
            color: theme.palette.getContrastText(deepOrange[500]),
            backgroundColor: '#196F3D',
            width: theme.spacing(7),
            height: theme.spacing(7),
        },
        ocupado: {
            color: theme.palette.getContrastText(deepOrange[500]),
            backgroundColor: deepOrange[500],
            width: theme.spacing(7),
            height: theme.spacing(7),
        },
        reservado: {
            color: theme.palette.getContrastText(deepOrange[500]),
            backgroundColor: deepOrange[500],
            width: theme.spacing(7),
            height: theme.spacing(7),
        },
    }));

    const classes = useStyles();
    const dispatch = useDispatch();
    const [mesaIdentificador, setMesaIdentificador] = useState('');
    const [categoria, setCategoria] = useState('');
    const [producto, setProducto] = useState('');
    const [cantidad, setCantidad] = useState(1);
    const dataRedux = useSelector((state) => state)

    const getCategories = () => {
        dispatch(categoriesAction.getCategoria());
    }
    const getProductos = () => {
        dispatch(productosAction.getProductos());
    }
    useEffect(() => {
        getCategories();
        getProductos();
    }, [])

    const state = useSelector((state) => state.mesas);
    const getMesas = () => {
        dispatch(mesaActions.getMesas());
    }

    useEffect(() => {
        getMesas();
    }, [])

    const mesaColor = (estado) => {
        if (estado === 1) {
            return classes.libre;
        }
        if (estado === 2) {
            return classes.ocupado;
        }
        if (estado === 3) {
            return classes.reservado;
        }
    }

    const mesainfo = async (mesa) => {
        console.log('ver mesa info: ', mesa)
        setMesaIdentificador("Mesa " + mesa.identificador)
        setCategoria('')
        setProducto('')
        if (mesa.estado !== 1) {
            dispatch(pedidosAction.getPedidoByMesaId(mesa.id))
        } else {
            modalForm.mesa = mesa.id
            setModalForm(DEFAULT_PEDIDO)
        }
    }

    const onSubmit = () => {
        function transformItems() {
            return modalForm.items.map((p) => {
                return {
                    "cantidad": p.cantidad,
                    "producto": {
                        "id": p.producto.id,
                        "precio": p.producto.precio
                    }
                }
            })
        }

        modalForm.mesero = currentUser.username;
        const request = {
            "id": modalForm.id,
            "dniCliente": modalForm.dniCliente,
            "estado": modalForm.estado,
            "mesa": {
                "id": modalForm.mesa
            },
            "mesero": {
                "mail": modalForm.mesero
            },
            "items": transformItems()
        }
        console.log(request)
        if (request.id) {
            dispatch(pedidosAction.editPedido(request))
        } else {
            dispatch(pedidosAction.savePedido(request))
        }
    }

    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }
    const handleChangeCategoria = (event) => {
        setCategoria(event.target.value)
        setProductoList(getProductosByCategoria(event.target.value.id))
    };

    const handleChangeProducto = (event) => {
        setProducto(event.target.value)
        setIdPedido(event.target.value.id)
    };

    function getProductosByCategoria(idCategoria) {
        return dataRedux.producto.list.data.filter((p) => p.categoria.id === idCategoria)
    }

    const onAddPedido = () => {
        function findNamePlato() {
            return dataRedux.producto.list.data.find((p) => p.id === idPedido)
        }

        const existsInPlato = modalForm.items.find((x) => x.producto.id === idPedido)
        const newArrayPedidos = [...modalForm.items]
        if (existsInPlato) {
            const newQty = existsInPlato.cantidad += cantidad
            const index = newArrayPedidos.findIndex((p) => p.producto.id === idPedido)
            newArrayPedidos[index]['cantidad'] = newQty
        } else {
            const p = {
                producto: findNamePlato(),
                cantidad: cantidad,
            }
            newArrayPedidos.push(p)
        }

        modalForm.items = newArrayPedidos
    }

    const renderPedidos = (rows) => {
        if (!rows) return false;
        return <TableContainer>
            <Table aria-label="spanning table">
                <TableHead>
                    <TableRow>
                        <TableCell align="center" colSpan={3}>
                            Detalle Pedido
                        </TableCell>
                        <TableCell align="right">Precio</TableCell>
                    </TableRow>
                    <TableRow>
                        <TableCell>Descripcion</TableCell>
                        <TableCell align="right">Cant.</TableCell>
                        <TableCell align="right">Und.</TableCell>
                        <TableCell align="right">Sum.</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                        <TableRow key={row.id}>
                            <TableCell>{row.producto.nombre}</TableCell>
                            <TableCell align="right">{row.cantidad}</TableCell>
                            <TableCell align="right">{row.producto.precio}</TableCell>
                            <TableCell align="right">{row.cantidad * row.producto.precio}</TableCell>
                            <TableCell className='tablePedidos__body--col'>
                                <IconButton color="inherit" onClick={() => null}>
                                    <RemoveCircleIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}

                </TableBody>
            </Table>
        </TableContainer>
    }

    const renderPedidosSection = (form) => {
        console.log(mesaIdentificador);
        // if (!form) {
        //     return <div> No seleccionaste ninguna mesa</div>
        // }
        return <div>
            <Label>{mesaIdentificador}</Label>

            <TextField
                margin="dense"
                label="DNI Cliente"
                id='dni'
                type="text"
                fullWidth
                value={form?.dniCliente}
                onChange={(val) => onUpdateInput(val, 'dniCliente')}
            />
            <br/>
            <br/>

            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                style={{width: '100%'}}
                value={categoria}
                placeholder="Seleccionar una opción"
                onChange={handleChangeCategoria}
            >
                {categoriaList.list.data && categoriaList.list.data.map((categoria) => (
                    <MenuItem value={categoria}
                              key={categoria.id}>{categoria.nombre} </MenuItem>
                ))}
            </Select>
            <br/>
            <br/>
            <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                style={{width: '100%'}}
                value={producto}
                placeholder="Seleccionar un Producto"
                onChange={handleChangeProducto}
            >
                {productoList.length === 0 && <MenuItem value={0}
                                                        key={0}>Seleccione una
                    opcion</MenuItem>}
                {productoList.length > 0 && productoList.map((producto) => (
                    <MenuItem value={producto}
                              key={producto.id}>{producto.nombre} </MenuItem>
                ))}
            </Select>
            <br/>
            <br/>
            <InputLabel id="demo-simple-select-filled-label">Cantidad</InputLabel>
            <InputSpinner
                type={'int'}
                precision={2}
                max={1000}
                min={0}
                step={1}
                value={1}
                onChange={num => setCantidad(num)}
                variant={'primary'}
                size="sm"
            />
            <br/>
            <br/>

            <Button color="primary" onClick={onAddPedido}>
                Agregar
            </Button>

            <Row>
                {form?.items?.length === 0 &&
                <div>Aún no agregaste ningún producto a tu pedido</div>}
                {form?.items?.length > 0 && renderPedidos(form?.items)}
            </Row>
        </div>

    }
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.mesa"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Grid container direction="row"
                          justify="space-between"
                          alignItems="stretch" spacing={1}>
                        <Grid item xs={12} md={7}>
                            <Row>
                                <Colxx xxs="12">
                                    <Card>
                                        <CardBody>
                                            <CardTitle>
                                                Control de Mesas
                                            </CardTitle>
                                            {state.list.loading && <LinearProgress/>}
                                            <Grid container spacing={3}>
                                                {state.list.data && state.list.data.map((data) => (

                                                    <Grid item xs={3}
                                                          key={data.id}>
                                                        <div onClick={() => mesainfo(data)}>
                                                            <Avatar
                                                                className={mesaColor(data.estado)}>
                                                                {data.identificador}
                                                            </Avatar>
                                                        </div>
                                                        <br/>
                                                        <DeckTwoTone style={{fontSize: 50}}/>
                                                    </Grid>

                                                ))}
                                            </Grid>
                                        </CardBody>
                                    </Card>
                                </Colxx>
                            </Row>
                        </Grid>
                        <Grid item xs={12} md={5}>
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        Pedido
                                    </CardTitle>
                                    <CardContent>
                                        {/* {mesaIdentificador === '' && <div> No seleccionaste ninguna mesa</div>}
                                        {mesaIdentificador !== '' && modalForm !== null
                                        && renderPedidosSection()} */}
                                        {renderPedidosSection(pedidoState.list.data)}
                                    </CardContent>
                                    <CardActions>
                                        {mesaIdentificador !== '' && modalForm.items.length > 0 &&
                                        <Button variant="contained" color="danger" onClick={onSubmit}>
                                            Guardar Pedido
                                        </Button>}
                                    </CardActions>
                                </CardBody>
                            </Card>
                        </Grid>
                    </Grid>
                </>
            </div>
        </AppLayout>
    );
}

export default Pedidos;
