import React, {useEffect, useState} from 'react';
import {connect, useDispatch, useSelector} from 'react-redux';
import AppLayout from '../../layout/AppLayout';
import {Card, CardBody, CardTitle, Row} from "reactstrap";
import {Colxx, Separator} from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import IntlMessages from "../../utils/helpers/IntlMessages";
import * as pedidoActions from "../../redux/pedido/actions";
import {
    Button,
    CardContent,
    Chip,
    Dialog,
    DialogActions,
    DialogContent, DialogContentText,
    DialogTitle, Fab,
    Grid, IconButton, InputLabel,
    LinearProgress, MenuItem,
    Paper, Select,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TablePagination,
    TableRow, TextField
} from "@material-ui/core";
import {Add, Block, Check, Edit} from "@material-ui/icons";
import {getCurrentUser} from "../../utils/helpers/Utils";
import * as categoriesAction from "../../redux/categoria/action";
import * as productosAction from "../../redux/producto/actions";
import * as userAction from "../../redux/users/actions";
import RemoveCircleIcon from "@material-ui/icons/RemoveCircle";
import InputSpinner from "react-bootstrap-input-spinner";
import * as pedidosAction from "../../redux/pedido/actions";

const DEFAULT_FORM = {
    id:'',
    dniCliente: '',
    nombreCliente: '',
    direccion: '',
    referencia: '',
    chofer: '',
    descripcion: '',
    mesero: '',
    estado: false,
    mesa: '',
    items: []
}
const DEFAULT_CHOFER = {
    nombre : 'Seleccionar una opcion'
}
const Delivery = () => {
    const dispatch = useDispatch();
    const categoriaList = useSelector((state) => state.categoria);
    const [categoria, setCategoria] = useState('');
    const [chofer, setChofer] = useState(DEFAULT_CHOFER);
    const [idChofer, setIdChofer] = useState('');
    const [producto, setProducto] = useState('');
    const [cantidad, setCantidad] = useState(1);
    const dataRedux = useSelector((state) => state);
    const [idPedido, setIdPedido] = useState('');
    const currentUser = getCurrentUser();
    const [productoList, setProductoList] = useState([]);
    const state = useSelector((state) => state.pedidosReducer);
    const stateUsers = useSelector((state) => state.users);
    const [openModal, setOpenModal] = useState(false)
    const [openModalForm, setOpenModalForm] = useState(false)
    const [detallePedido, setDetallePedido] = useState([])
    const [rowsPerPage, setRowsPerPage] = React.useState(10)
    const [page, setPage] = React.useState(0)
    const [modalForm, setModalForm] = useState(DEFAULT_FORM)
    const [openModalConfirmation, setOpenModalConfirmation] = useState(false)
    const [total, setTotal] = useState(0)
    const [detalleItems, setDetalleItems] = useState([])
    const getCategories = () => {
        dispatch(categoriesAction.getCategoria());
    }
    const getProductos = () => {
        dispatch(productosAction.getProductos());
    }
    const getUsers = () => {
        dispatch(userAction.getUsers());
    }
    const userList = () => {
        if (stateUsers.list.data !== null) {
            return stateUsers.list.data.filter((u) => u.roles[0].nombre === "CHOFER");
        }
    }
    const pedidosDeliveryList = () => {
        if (state.list.data !== null) {
            return state.list.data.filter((pedido) => pedido.tipo === 2)
        }
    }
    const getPedidos = () => {
        dispatch(pedidoActions.getPedidos());
    }
    const handleChangeCategoria = (event) => {
        setCategoria(event.target.value)
        setProductoList(getProductosByCategoria(event.target.value.id))
    };

    const handleChangeChofer = (event) => {
        setChofer(event.target.value)
        setIdChofer(event.target.value.id)
    };

    const hableCloseModalConfirmation = () => {
        setOpenModalConfirmation(false);
    }

    function getProductosByCategoria(idCategoria) {
        return dataRedux.producto.list.data.filter((p) => p.categoria.id === idCategoria)
    }

    const handleChangeProducto = (event) => {
        setProducto(event.target.value)
        setIdPedido(event.target.value.id)
    };
    useEffect(() => {
        getPedidos();
        getCategories();
        getProductos();
        getUsers();
    }, [])

    const getTotal = (items) => {
        return items.reduce((acc, el) => {
            return (acc += (el.cantidad * el.producto.precio))
        }, 0);
    }

    const getTotalPlatos = (items) => {
        return items.reduce((acc, el) => {
            return (acc += (el.cantidad))
        }, 0);
    }
    const handleCloseModal = () => {
        setOpenModal(false)
    }
    const handleCloseModalForm = () => {
        setOpenModalForm(false)
    }
    const handleChangePage = (event, newPage) => {
        setPage(newPage);
    };

    const handleChangeRowsPerPage = (event) => {
        setRowsPerPage(event.target.value);
        setPage(0);
    };
    const onOpenModal = (items) => {
        setDetallePedido(items)
        setOpenModal(true)
        setTotal(getTotal(items))
    }
    const onOpenModalForm = () => {
        setProducto('')
        setCategoria('')
        setDetalleItems([])
        setOpenModalForm(true)
        setModalForm(DEFAULT_FORM)
    }
    const onUpdateInput = (val, id) => {
        setModalForm((state) => {
            return {
                ...state,
                [id]: val.target.value
            }
        })
    }
    const onAddPedido = () => {
        function findNamePlato() {
            return dataRedux.producto.list.data.find((p) => p.id === idPedido)
        }

        const existsInPlato = detalleItems.find((x) => x.producto.id === idPedido)
        const newArrayPedidos = [...detalleItems]
        if (existsInPlato) {
            const newQty = existsInPlato.cantidad += cantidad
            const index = newArrayPedidos.findIndex((p) => p.producto.id === idPedido)
            newArrayPedidos[index]['cantidad'] = newQty
        } else {
            const p = {
                producto: findNamePlato(),
                cantidad: cantidad,
            }
            newArrayPedidos.push(p)
        }
        setDetalleItems(newArrayPedidos)
        //modalForm.items = newArrayPedidos
    }
    const onDeleteItem = (pro) => {
        const newList = detalleItems.filter((p) => p.producto.id !== pro.producto.id);
        setDetalleItems(newList);
    }
    const onSubmit = () => {
        function transformItems() {
            return modalForm.items.map((p) => {
                return {
                    "cantidad": p.cantidad,
                    "producto": {
                        "id": p.producto.id,
                        "precio": p.producto.precio
                    }
                }
            })
        }

        modalForm.mesero = currentUser.username;
        modalForm.items = detalleItems;
        const request = {
            "id": modalForm.id,
            "dniCliente": modalForm.dniCliente,
            "tipo": 2,
            "nombreCliente": modalForm.nombreCliente,
            "direccion": modalForm.direccion,
            "referencia": modalForm.referencia,
            "descripcion": modalForm.descripcion,
            "estado": modalForm.estado,
            "chofer": {
                "id": chofer.id
            },
            "mesero": {
                "mail": modalForm.mesero
            },
            "items": transformItems()
        }
        if (request.id) {
            dispatch(pedidosAction.editPedido(request))
        } else {
            dispatch(pedidosAction.savePedido(request))
        }
        handleCloseModalForm();
        setModalForm(DEFAULT_FORM)
    }
    const onEdit = (data) => {
        setDetalleItems(data.items)
        setModalForm(data)
        setOpenModalForm(true)
    }
    const pagarPedido = () => {
        dispatch(pedidosAction.payPedido(idPedido, state))
        setOpenModalConfirmation(false)
    }
    const openConfirmation = (idPedido) => {
        setIdPedido(idPedido)
        setOpenModalConfirmation(true)
    }
    const getChofer = () => {
        return modalForm.chofer? userList().filter((c) => c.id === modalForm.chofer.id):chofer;
    }
    const renderPedidos = () => {
        return (<TableContainer>
            <Table aria-label="spanning table">
                <TableHead>
                    <TableRow key={"head1"}>
                        <TableCell align="center" colSpan={3}>
                            Detalle Pedido
                        </TableCell>
                        <TableCell align="right">Precio</TableCell>
                    </TableRow>
                    <TableRow key={"head2"}>
                        <TableCell>Descripcion</TableCell>
                        <TableCell align="right">Cant.</TableCell>
                        <TableCell align="right">Und.</TableCell>
                        <TableCell align="right">Sum.</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {detalleItems.map((row) => (
                        <TableRow key={row.producto.id}>
                            <TableCell>{row.producto.nombre}</TableCell>
                            <TableCell align="right">{row.cantidad}</TableCell>
                            <TableCell align="right">{row.producto.precio}</TableCell>
                            <TableCell align="right">{row.cantidad * row.producto.precio}</TableCell>
                            <TableCell className='tablePedidos__body--col'>
                                <IconButton color="inherit" edge="end" aria-label="delete"
                                            onClick={() => onDeleteItem(row)}>
                                    <RemoveCircleIcon/>
                                </IconButton>
                            </TableCell>
                        </TableRow>
                    ))}

                </TableBody>
            </Table>
        </TableContainer>)
    }
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.delivery" />
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12" className="mb-4">
                            <p>
                                <IntlMessages id="menu.delivery"/>
                            </p>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de Delivery
                                            </Grid>
                                            <Grid item xs={6} sm={4}>
                                                <Fab style={{float: 'right'}} color="primary" aria-label="add"
                                                     onClick={onOpenModalForm}>
                                                    <Add/>
                                                </Fab>
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {!pedidosDeliveryList() && <LinearProgress/>}
                                        <Paper>
                                            <TableContainer>
                                                <Table stickyHeader aria-label="sticky table">
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell key={'dniCliente'} style={{minWidth: '170'}}>
                                                                DNI Cliente
                                                            </TableCell>

                                                            <TableCell key={'mesa'} style={{minWidth: '170'}}>
                                                                Nombre Cliente
                                                            </TableCell>

                                                            <TableCell key={'direccion'} style={{minWidth: '170'}}>
                                                                Direccion
                                                            </TableCell>

                                                            <TableCell key={'referencia'} style={{minWidth: '170'}}>
                                                                Referencia
                                                            </TableCell>

                                                            <TableCell key={'chofer'} style={{minWidth: '170'}}>
                                                                Chofer
                                                            </TableCell>

                                                            <TableCell key={'productos'} style={{minWidth: '170'}}>
                                                                Productos
                                                            </TableCell>

                                                            <TableCell key={'total'} style={{minWidth: '170'}}>
                                                                Total
                                                            </TableCell>

                                                            <TableCell key={'estado'} style={{minWidth: '170'}}>
                                                                Estado
                                                            </TableCell>

                                                            <TableCell key={'accion'} style={{minWidth: '170'}}>
                                                                Accion
                                                            </TableCell>

                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {pedidosDeliveryList() && pedidosDeliveryList().map((row) => {
                                                            return (
                                                                <TableRow key={row.id}>
                                                                    <TableCell>{row.dniCliente}</TableCell>
                                                                    <TableCell> {row.nombreCliente}</TableCell>
                                                                    <TableCell> {row.direccion}</TableCell>
                                                                    <TableCell> {row.referencia}</TableCell>
                                                                    <TableCell> {row.chofer.nombre}</TableCell>
                                                                    <TableCell>
                                                                        <Chip
                                                                            onClick={() => onOpenModal(row.items)}
                                                                            label={getTotalPlatos(row.items)} color="primary"/>
                                                                    </TableCell>
                                                                    <TableCell>
                                                                        {getTotal(row.items)}
                                                                    </TableCell>
                                                                    <TableCell>
                                                                        {row.estado ? <Chip
                                                                            label="Pagado"
                                                                            color="primary"
                                                                            deleteIcon={<Check/>}
                                                                        /> : <IconButton
                                                                            onClick={() => openConfirmation(row.id)}>
                                                                            <Block/>
                                                                        </IconButton>}
                                                                    </TableCell>
                                                                    <TableCell>
                                                                        <IconButton edge="end"
                                                                                    disabled={row.estado}
                                                                                    aria-label="edit"
                                                                                    onClick={() => onEdit(row)}>
                                                                            <Edit/>
                                                                        </IconButton>
                                                                    </TableCell>
                                                                </TableRow>
                                                            );
                                                        })}

                                                    </TableBody>
                                                </Table>
                                            </TableContainer>
                                            <TablePagination
                                                rowsPerPageOptions={[10, 25, 100]}
                                                component="div"
                                                count={100}
                                                rowsPerPage={rowsPerPage}
                                                page={page}
                                                onChangePage={handleChangePage}
                                                onChangeRowsPerPage={handleChangeRowsPerPage}
                                            />
                                        </Paper>
                                    </CardContent>
                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
            <Dialog open={openModal} maxWidth='xs' onClose={handleCloseModal} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    Detalle Pedido
                </DialogTitle>
                <DialogContent>
                    <TableContainer>
                        <Table stickyHeader aria-label="sticky table">
                            <TableHead>
                                <TableRow>
                                    <TableCell key={'cantidad'} style={{minWidth: '170'}}>
                                        Cantidad
                                    </TableCell>
                                    <TableCell key={'desc'} style={{minWidth: '170'}}>
                                        Desc
                                    </TableCell>
                                    <TableCell key={'un'} style={{minWidth: '170'}}>
                                        Prec. Un
                                    </TableCell>
                                    <TableCell key={'un'} style={{minWidth: '170'}}>
                                        SubTotal
                                    </TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {detallePedido.map((row) => {
                                    return (
                                        <TableRow key={row.producto.id}>
                                            <TableCell>
                                                {row.cantidad}
                                            </TableCell>
                                            <TableCell>
                                                {row.producto.nombre}
                                            </TableCell>
                                            <TableCell>
                                                {row.producto.precio}
                                            </TableCell>
                                            <TableCell>
                                                {row.producto.precio * row.cantidad}
                                            </TableCell>
                                        </TableRow>
                                    );
                                })}
                                <TableRow>
                                    <TableCell rowSpan={3} />
                                    <TableCell colSpan={2}>Total</TableCell>
                                    <TableCell align="right">{total}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseModal} color="primary">
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
            <Dialog open={openModalForm} maxWidth='xs' onClose={handleCloseModalForm}
                    aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">
                    {modalForm.id ? 'Editar' : 'Agregar'} Pedido Delivery
                </DialogTitle>
                <DialogContent>

                    <TextField
                        margin="dense"
                        label="DNI Cliente"
                        id='dni'
                        type="text"
                        fullWidth
                        value={modalForm.dniCliente}
                        onChange={(val) => onUpdateInput(val, 'dniCliente')}
                    />
                    <TextField
                        margin="dense"
                        label="Nombre Cliente"
                        id='nombre'
                        type="text"
                        fullWidth
                        value={modalForm.nombreCliente}
                        onChange={(val) => onUpdateInput(val, 'nombreCliente')}
                    />
                    <TextField
                        margin="dense"
                        label="Direccion"
                        id='direccion'
                        type="text"
                        fullWidth
                        value={modalForm.direccion}
                        onChange={(val) => onUpdateInput(val, 'direccion')}
                    />
                    <TextField
                        id="referencia"
                        label="Referencia"
                        multiline
                        fullWidth
                        rows={2}
                        value={modalForm.referencia}
                        onChange={(val) => onUpdateInput(val, 'referencia')}
                    />
                    <TextField
                        id="descripcion"
                        label="Descripcion"
                        multiline
                        fullWidth
                        rows={2}
                        value={modalForm.descripcion}
                        onChange={(val) => onUpdateInput(val, 'descripcion')}
                    />
                    <br/>
                    <br/>
                    <InputLabel id="demo-simple-select-filled-label">Chofer</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        style={{width: '100%'}}
                        value={getChofer}
                        placeholder="Seleccionar una opción"
                        onChange={handleChangeChofer}
                    >
                        {userList() && userList().map((chofer) => (
                            <MenuItem value={chofer}
                                      key={chofer.id}>{chofer.nombre} {chofer.apellidoPaterno} </MenuItem>
                        ))}
                    </Select>
                    <br/>
                    <br/>
                    <InputLabel id="demo-simple-select-filled-label">Categoria</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        style={{width: '100%'}}
                        value={categoria}
                        placeholder="Seleccionar una opción"
                        onChange={handleChangeCategoria}
                    >
                        {categoriaList.list.data && categoriaList.list.data.map((categoria) => (
                            <MenuItem value={categoria}
                                      key={categoria.id}>{categoria.nombre} </MenuItem>
                        ))}
                    </Select>
                    <br/>
                    <br/>
                    <InputLabel id="demo-simple-select-filled-label">Producto</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        style={{width: '100%'}}
                        value={producto}
                        placeholder="Seleccionar un Producto"
                        onChange={handleChangeProducto}
                    >
                        {productoList.length === 0 && <MenuItem value={0}
                                                                key={0}>Seleccione una
                            opcion</MenuItem>}
                        {productoList.length > 0 && productoList.map((producto) => (
                            <MenuItem value={producto}
                                      key={producto.id}>{producto.nombre} </MenuItem>
                        ))}
                    </Select>
                    <br/>
                    <br/>
                    <InputLabel id="demo-simple-select-filled-label">Cantidad</InputLabel>
                    <InputSpinner
                        type={'int'}
                        precision={2}
                        max={1000}
                        min={0}
                        step={1}
                        value={1}
                        onChange={num => setCantidad(num)}
                        variant={'primary'}
                        size="sm"
                    />
                    <br/>
                    <br/>

                    <Button color="secondary" onClick={onAddPedido}>
                        Agregar
                    </Button>

                    <Row>
                        {detalleItems.length === 0 &&
                        <div>Aún no agregaste ningún producto a tu pedido</div>}
                        {detalleItems.length > 0 && renderPedidos()}
                    </Row>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleCloseModalForm} color="primary">
                        Cancelar
                    </Button>
                    <Button variant="contained" color="primary" onClick={onSubmit}>
                        Guardar
                    </Button>
                </DialogActions>
            </Dialog>

            <Dialog fullWidth={true}
                    maxWidth='xs'
                    open={openModalConfirmation}
                    onClose={hableCloseModalConfirmation}
                    aria-labelledby="max-width-dialog-title">
                <DialogTitle id="alert-dialog-title">{"Esta seguro que desea cerrar este pedido?"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Asegurese que lo cobrado sea lo correcto y
                        revisar bien las monedas y billetes antes de dar por
                        concluido el pago

                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={hableCloseModalConfirmation} color="primary">
                        cancelar
                    </Button>
                    <Button onClick={() => pagarPedido()} color="primary" autoFocus>
                        Aceptar
                    </Button>
                </DialogActions>
            </Dialog>
        </AppLayout>
    );
};



export default Delivery;
