import React, {useState, useEffect} from 'react';
import {Row, Card, CardTitle, Label, FormGroup, Button} from 'reactstrap';
import {connect} from 'react-redux';
import {Formik, Form, Field} from 'formik';
import {NotificationManager} from '../../components/common/react-notifications';
import {loginUser} from '../../redux/auth/actions';
import {Colxx} from '../../components/common/CustomBootstrap';
import IntlMessages from '../../utils/helpers/IntlMessages';
import UserLayout from "../../layout/UserLayout";

const validatePassword = (value) => {
    let error;
    if (!value) {
        error = 'el campo de contraseña, no puede estar vacia';
    } else if (value.length < 4) {
        error = 'el valor ingresado debe de ser mayor que 3 caracteres';
    }
    return error;
};

const validateEmail = (value) => {
    let error;
    if (!value) {
        error = 'el campo de mail, no puede estar vacio';
    } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value)) {
        error = 'El mail ingresado no es correcto!';
    }
    return error;
};


const Login = ({history,loading, error, loginUserAction}) => {
    const [mail] = useState('');
    const [password] = useState('');

    useEffect(() => {
        if (error) {
            NotificationManager.error(error, 'Login Error', 3000, null, null, '');
        }
    }, [error]);

    const onUserLogin = (values) => {
        if (!loading) {
            if (values.mail !== '' && values.password !== '') {
                values.history = history;
                loginUserAction(values);
            }
        }
    };

    const initialValues = {mail, password};
    return (
        <UserLayout>
            <Row className="h-100">
                <Colxx xxs="12" md="10" className="mx-auto my-auto">
                    <Card className="auth-card">
                        <div className="position-relative image-side ">
                        </div>
                        <div className="form-side">
                            <CardTitle className="mb-4">
                                <IntlMessages id="user.login-title"/>
                            </CardTitle>
                            <Formik initialValues={initialValues} onSubmit={onUserLogin}>
                                {({errors, touched}) => (
                                    <Form className="av-tooltip tooltip-label-bottom">
                                        <FormGroup className="form-group has-float-label">
                                            <Label>
                                                <IntlMessages id="user.email"/>
                                            </Label>
                                            <Field
                                                className="form-control"
                                                name="mail"
                                                validate={validateEmail}
                                            />
                                            {errors.mail && touched.mail && (
                                                <div className="invalid-feedback d-block">
                                                    {errors.mail}
                                                </div>
                                            )}
                                        </FormGroup>
                                        <FormGroup className="form-group has-float-label">
                                            <Label>
                                                <IntlMessages id="user.password"/>
                                            </Label>
                                            <Field
                                                className="form-control"
                                                type="password"
                                                name="password"
                                                validate={validatePassword}
                                            />
                                            {errors.password && touched.password && (
                                                <div className="invalid-feedback d-block">
                                                    {errors.password}
                                                </div>
                                            )}
                                        </FormGroup>
                                        <div className="d-flex justify-content-between align-items-center">

                                            <Button
                                                color="primary"
                                                className={`btn-shadow btn-multiple-state ${
                                                    loading ? 'show-spinner' : ''
                                                }`}
                                                size="lg"
                                            >
                      <span className="spinner d-inline-block">
                        <span className="bounce1"/>
                        <span className="bounce2"/>
                        <span className="bounce3"/>
                      </span>
                                                <span className="label">
                        <IntlMessages id="user.login-button"/>
                      </span>
                                            </Button>
                                        </div>
                                    </Form>
                                )}
                            </Formik>
                        </div>
                    </Card>
                </Colxx>
            </Row>
        </UserLayout>

    );
};
const mapStateToProps = ({authUser}) => {
    const {loading, error} = authUser;
    return {loading, error};
};

export default connect(mapStateToProps, {
    loginUserAction: loginUser,
})(Login);
