import React, {useEffect, useState} from 'react';
import {Card, CardBody, CardTitle, Row} from 'reactstrap';
import {Colxx, Separator} from '../../components/common/CustomBootstrap';
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import {useDispatch, useSelector} from "react-redux";
import AppLayout from "../../layout/AppLayout";
import cols from "../../utils/constants/categoriaHeaders"
import * as mesaActions from "../../redux/mesa/actions";
import {CardContent, Fab, Grid, LinearProgress} from "@material-ui/core";
import {Add} from "@material-ui/icons";
import TableMesa from "../../utils/tables/table-mesa";
import ModalMesa from "../../utils/modals/mesa-modal";

function Mesas() {
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = useState(false)

    let headers = React.useMemo(
        () => cols, []
    );
    const state = useSelector((state) => state.mesas);
    const getMesas = () => {
        dispatch(mesaActions.getMesas());
    }
    useEffect(() => {
        getMesas();
    }, [])

    const onOpenModalMesa = () => {
        setOpenModal(true)
    }
    const onCloseModalMesa = () => {
        setOpenModal(false)
    }

    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.mesa"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista Mesas
                                            </Grid>
                                            <Grid item xs={6} sm={4}>
                                                <Fab style={{float: 'right'}}
                                                     color="primary"
                                                     aria-label="add"
                                                     onClick={onOpenModalMesa}>
                                                    <Add/>
                                                </Fab>
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {state.list.loading && <LinearProgress/>}
                                        { state.list.data &&
                                          state.list.data.length > 0 ?
                                            <TableMesa columns={headers} data={state}/> :
                                            <div> no hay data</div>}
                                    </CardContent>
                                </CardBody>
                            </Card>
                        </Colxx>
                    </Row>
                </>
            </div>
            <ModalMesa openModal={openModal} onCloseModal={onCloseModalMesa}/>
        </AppLayout>

    )
        ;
}

export default Mesas;
