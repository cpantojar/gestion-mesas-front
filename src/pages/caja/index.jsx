import React, {useEffect} from 'react';
import {withRouter} from 'react-router-dom';
import {connect, useDispatch, useSelector} from 'react-redux';

import AppLayout from '../../layout/AppLayout';
import {Card, CardBody, CardTitle, Row} from "reactstrap";
import {Colxx, Separator} from "../../components/common/CustomBootstrap";
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import IntlMessages from "../../utils/helpers/IntlMessages";
import * as pedidoActions from "../../redux/pedido/actions";
import {CardContent, Grid, LinearProgress} from "@material-ui/core";
import TablePedido from "../../utils/tables/table-pedido";

const Caja = ({match}) => {
    const dispatch = useDispatch();
    const state = useSelector((state) => state.pedidosReducer);
    const getPedidos = () => {
        dispatch(pedidoActions.getPedidos());
    }
    useEffect(() => {
        getPedidos();
    }, [])
    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.caja" match={match}/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12" className="mb-4">
                            <p>
                                <IntlMessages id="menu.caja"/>
                            </p>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de Pedidos
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {!state.list.data && <LinearProgress/>}
                                        {state.list.data && state.list.data.length > 0 ?
                                            <TablePedido data={state}/> :
                                            <div> no hay data</div>}
                                    </CardContent>
                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
        </AppLayout>
    );
};

const mapStateToProps = ({menu}) => {
    const {containerClassnames} = menu;
    return {containerClassnames};
};

export default withRouter(connect(mapStateToProps, {})(Caja));
