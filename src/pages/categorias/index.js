import React, {useEffect, useState} from 'react';
import {Card, CardBody, CardTitle, Row} from 'reactstrap';
import {Colxx, Separator} from '../../components/common/CustomBootstrap';
import Breadcrumb from "../../components/containers/navs/Breadcrumb";
import {useDispatch, useSelector} from "react-redux";
import AppLayout from "../../layout/AppLayout";
import cols from "../../utils/constants/categoriaHeaders"
import * as categoriaActions from "../../redux/categoria/action";
import TableCategoria from "../../utils/tables/table-categoria";
import {CardContent, Fab, Grid, LinearProgress} from "@material-ui/core";
import {Add} from "@material-ui/icons";
import ModalCategoria from "../../utils/modals/categoria-modal";

function Categorias() {
    const dispatch = useDispatch();
    const [openModal, setOpenModal] = useState(false)

    let headers = React.useMemo(
        () => cols, []
    );
    const state = useSelector((state) => state.categoria);
    const getCategoria = () => {
        dispatch(categoriaActions.getCategoria());
    }
    useEffect(() => {
        getCategoria();
    }, [])

    const onOpenModalCategoria = () => {
        setOpenModal(true)
    }
    const onCloseModalCategoria = () => {
        setOpenModal(false)
    }

    return (
        <AppLayout>
            <div className="dashboard-wrapper">
                <>
                    <Row>
                        <Colxx xxs="12">
                            <Breadcrumb heading="menu.categorias"/>
                            <Separator className="mb-5"/>
                        </Colxx>
                    </Row>
                    <Row>
                        <Colxx xxs="12">
                            <Card>
                                <CardBody>
                                    <CardTitle>
                                        <Grid container spacing={2}>
                                            <Grid item xs={6} sm={6}>
                                                Lista de Categoria
                                            </Grid>
                                            <Grid item xs={6} sm={4}>
                                                <Fab style={{float: 'right'}}
                                                     color="primary"
                                                     aria-label="add"
                                                     onClick={onOpenModalCategoria}>
                                                    <Add/>
                                                </Fab>
                                            </Grid>
                                        </Grid>
                                    </CardTitle>
                                    <CardContent>
                                        {!state.list.data && <LinearProgress />}
                                        {  state.list.data && state.list.data.length > 0?
                                            <TableCategoria columns={headers} data={state}/> :
                                            <div> no hay data</div>}
                                    </CardContent>

                                </CardBody>
                            </Card>
                        </Colxx>

                    </Row>
                </>
            </div>
            <ModalCategoria openModal={openModal} onCloseModal={onCloseModalCategoria}/>
        </AppLayout>

    )
        ;
}

export default Categorias;
